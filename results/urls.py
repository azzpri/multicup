from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^vote/(?P<challenge_pk>[0-9]+)$', views.vote, name='vote'),
    url(r'^like/$', views.add_like, name='like'),    
]