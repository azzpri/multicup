import datetime, json
from django.utils import timezone
from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User
from multicup.settings import SERVER_URL
from members.models import Member, Juror
from challenges.models import Challenge
from tasks.models import Task
from results.models import Result, ResultVote

def responseContentToData(content):
    answer = content.decode('utf-8')
    return json.loads(answer)    

class ResultTestCase(TestCase):
    def setUp(self):           
        task = Task.objects.create(
            title="Тестовая задача 1",
            short_description="Короткое описание тестовой задачи",
            full_description="Полное описание тестовой задачи",
            cost=300,
            completed=20,
            average_vote=10,
            duration=20,
            materials="http://testserver.cpm/docs"
        )
        
        user = User.objects.create(username='testusername123')
        member = Member.objects.create(user = user, amount=1000)
        challenge_end_time = timezone.now() + datetime.timedelta(hours=task.duration)
        challenge = Challenge.objects.create(task=task, member=member, status='initial', time_limit=challenge_end_time)
        result = Result.objects.create(challenge=challenge, task=task, member=member, description='Описание Результата')
        juror = Juror.objects.create(member=member)
        resultvote = ResultVote.objects.create(result=result, juror=juror)
        
    # Проверяем оценку Результата
    def test_vote_result(self):
        result = Result.objects.get(description='Описание Результата')
        user = User.objects.get(username='testusername123')
        client = Client()
        client.force_login(user)
        response = client.post(
            SERVER_URL + 'results/vote/' + str(result.id),
            {
                'vote1' : 5,
                'vote2' : 5,
                'vote3' : 5,
                'vote4' : 5,
                'vote5' : 5,
                'comment' : 'Комментарий',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        data = responseContentToData(response.content)
        self.assertEqual(data['status'], 1)
        
        member = Member.objects.get(user = user)
        juror = Juror.objects.get(member=member)
        resultvote = ResultVote.objects.get(result=result, juror=juror)
        self.assertEqual(resultvote.average_vote, 5)
        self.assertEqual(resultvote.comment, 'Комментарий')
        
    # Проверяем лайк Результата
    def test_result_like(self):
        pass