from django.contrib import admin
from .models import Result, ResultVote, ResultLikeHistory
from challenges.models import Challenge

class ResultAdmin(admin.ModelAdmin):
    list_display = ('task_title', 'title', 'challenge_pk', 'username', 'created_at')
    readonly_fields = ( 'title', 'task_title', 'challenge_pk', 'username', 'created_at', 'description', 'url_title', 'url', 'image', 'likes')
    search_fields = ['member__user__first_name', 'member__user__last_name', 'member__user__username', 'title', 'challenge__task__title', 'challenge__id']
    exclude = ('task', 'challenge', 'member')
    
    def username(self, obj):
        return obj.member.user.first_name + " " + obj.member.user.last_name + " (" + obj.member.user.username + ")"
    
    username.short_description = 'Пользователь'

    def task_title(self, obj):
        return obj.task.title
    
    task_title.short_description = 'Задача'
     
    def challenge_pk(self, obj):
        return obj.challenge.pk
    
    challenge_pk.short_description = 'Соревнование'
    
admin.site.register(Result, ResultAdmin)

class ResultVoteAdmin(admin.ModelAdmin):
    readonly_fields = ('result_pk', 'juror_username', 'voted', 'vote1', 'vote2', 'vote3', 'vote4', 'vote5', 'average_vote', 'comment','created_at')
    list_display = ('result_pk', 'result_task_title', 'juror_username', 'voted', 'vote1', 'vote2', 'vote3', 'vote4', 'vote5', 'average_vote')
    search_fields = ['juror__member__user__first_name', 'juror__member__user__last_name', 'juror__member__user__username']
    exclude = ('result', 'juror')
    
    def result_pk(self, obj):
        return obj.result.pk
    
    result_pk.short_description = 'Результат'
    
    def result_task_title(self, obj):
        return obj.result.task.title
    
    result_task_title.short_description = 'Задача'
    
    def juror_username(self, obj):        
        return obj.juror.member.user.first_name + " " + obj.juror.member.user.last_name + " (" + obj.juror.member.user.username + ")"
    
admin.site.register(ResultVote, ResultVoteAdmin)

class ResultLikeHistoryAdmin(admin.ModelAdmin):
    readonly_fields = ('result_pk', 'result_task_title', 'member_username', 'user', 'created_at')
    list_display = ('result_pk', 'result_task_title', 'member_username', )
    list_filter = ('result__id', 'result__member__user__last_name',)
    
    def result_pk(self, obj):
        return obj.result.pk
    
    result_pk.short_description = 'Результат'
    
    def result_task_title(self, obj):
        return obj.result.task.title
    
    result_task_title.short_description = 'Задача'
    
    def member_username(self, obj):        
        return obj.result.member.user.first_name + " " + obj.result.member.user.last_name + " (" + obj.result.member.user.username + ")"

    result_task_title.short_description = 'Участник'
    
admin.site.register(ResultLikeHistory, ResultLikeHistoryAdmin)    