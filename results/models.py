import challenges, front
from challenges.models import Challenge
from django.db import models
from members.models import Member, Juror
from tasks.models import Task

# Результат - это материалы, которые загрузил пользователь во время испытания
class Result(models.Model):
    
    class Meta:
        verbose_name = 'Результат'
        verbose_name_plural = 'Результаты'
        
    task = models.ForeignKey(Task, on_delete=models.CASCADE, verbose_name='Задача')
    challenge = models.ForeignKey(Challenge, on_delete=models.CASCADE, verbose_name='Соревнование')
    member = models.ForeignKey(Member, on_delete=models.CASCADE, verbose_name='Участник')
    description = models.TextField(verbose_name='Описание')
    image = models.FileField(verbose_name='Изображение', upload_to = "uploads/materials/")
    title = models.CharField(max_length=100, verbose_name='Название')
    url = models.CharField(max_length=150, verbose_name='Ссылка')
    url_title = models.CharField(max_length=100, verbose_name='Название cсылки')
    likes = models.IntegerField(default=0, verbose_name='Лайков')
    created_at = models.DateTimeField(auto_now_add=True, blank=True, verbose_name="Создано")
    
    def __str__(self):
        return str(self.title)
    
# Голосования за Результаты
class ResultVote(models.Model):
    
    class Meta:
        verbose_name = 'Голосования'
        verbose_name_plural = 'Голосования'
            
    result = models.ForeignKey(Result, on_delete=models.CASCADE, verbose_name='Результат')
    juror = models.ForeignKey(Juror, on_delete=models.CASCADE, verbose_name='Жюри')
    voted = models.BooleanField(default=0, verbose_name='Голосовал ли')
    vote1 = models.IntegerField(default=0, verbose_name='Логика изложения')
    vote2 = models.IntegerField(default=0, verbose_name='Качество реализации')
    vote3 = models.IntegerField(default=0, verbose_name='Значимость для целевой аудитории')
    vote4 = models.IntegerField(default=0, verbose_name='Нестандартность решения')
    vote5 = models.IntegerField(default=0, verbose_name='Баланс форм и содержания')
    average_vote = models.FloatField(default=0, verbose_name='Средний балл')
    comment = models.TextField(blank=True, verbose_name='Комментарий')
    created_at = models.DateTimeField(auto_now_add=True, blank=True, verbose_name="Создано")
    
# История лайков
class ResultLikeHistory(models.Model):
    
    class Meta:
        verbose_name = 'Лайк за результат'
        verbose_name_plural = 'Лайки за результаты'
            
    result = models.ForeignKey(Result, on_delete=models.CASCADE, verbose_name='Результат')
    user = models.CharField(max_length=30, verbose_name='ID пользователя')
    created_at = models.DateTimeField(auto_now_add=True, blank=True, verbose_name="Создано")


# Получить список материалов, доступных для проверки жюри
def get_materials_for_help(juror):
    result_vote_limit = front.models.Setting.objects.filter(key = 'result_vote_limit').first()
    if result_vote_limit and int(result_vote_limit.value) > 1:        
        return [challenges.views.challenge_to_material(x) for x in Challenge.objects.raw('''
            select distinct
                ch.id
            from 
                (   select
                        id, result_id, count(result_id) as count
                    from
                        results_resultvote
                    where
                        result_id not in (
                            select
                                result_id
                            from
                                results_resultvote
                            where
                                juror_id = %s
                        )
                    group by result_id
                ) rv,
                results_result r,
                tasks_task t,
                challenges_challenge ch
            where 
                rv.result_id = r.id
                and t.id = r.task_id
                and t.format in %s
                and ch.id = r.challenge_id and count < %s''',
            [juror.id, tuple(juror.formats), result_vote_limit.value]
        )]
    return []

# Проверить, может ли жюри оценивать данное задание
def can_vote_for_material(juror, result):
    resultvotes = ResultVote.objects.filter(result = result)    
    jurorvotes = resultvotes.filter(juror = juror).first()
    try:
        result_vote_limit = int(front.models.Setting.objects.filter(key = 'result_vote_limit').first().value)
    except:
        result_vote_limit = 100
    if jurorvotes and resultvotes.count() <= result_vote_limit:
        return not jurorvotes.voted

    if result_vote_limit > resultvotes.filter(voted=True).count() and result.challenge.task.format in juror.formats:
        return True

    return False