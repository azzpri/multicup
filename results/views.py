import json, urllib, front
from challenges.models import Challenge
from challenges.views import checkAmounts
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.utils import timezone
from front.templatetags.multicup import declension
from members.models import Juror
from multicup.settings import MULTICUP, SECRET_KEY
from results.models import Result, ResultVote, ResultLikeHistory, can_vote_for_material
from tasks.models import FORMAT_CHOICES

def check_vote(vote):
    if vote and len(vote) > 0 and isinstance(int(vote), int):
        return True
    return False
    
# Оценить задание
def vote(request, challenge_pk):    
    response = {'status': 0} 
    
    if front.views.CUP_DATES['rating_change_finish'] and \
        front.views.CUP_DATES['rating_change_finish'] < timezone.now():
            response['error'] = 'Конкурс окончен. Новые оценки не принимаются'
    else:
        if  not check_vote(request.POST.get("vote_1")) or not check_vote(request.POST.get("vote_2")) or \
            not check_vote(request.POST.get("vote_3")) or not check_vote(request.POST.get("vote_4")) or \
            not check_vote(request.POST.get("vote_5")) or not request.POST.get("comment"):
             
            response['error'] = 'Неверно заполнены данные'
        else:        
            if not request.user.is_authenticated():
                response['error'] = 'Пользователь не авторизован'            
            else:
                juror = Juror.objects.filter(member__user = request.user).first()
                
                if not juror:
                    response['error'] = 'Пользователь не является жюри'
                else:
                    if not can_vote_for_material(juror, Result.objects.filter(challenge__pk = challenge_pk).last()):
                        response['error'] = 'Оценок для данного материала уже достаточно'
                    else:   
                        resultvote = ResultVote.objects.filter(result__challenge__pk = challenge_pk, juror = juror).first()
                    
                        if not resultvote:
                            result = Result.objects.filter(challenge__id = challenge_pk).order_by('-created_at').first()
                            resultvote = ResultVote.objects.create(juror = juror, result = result)
                                                    
                        resultvote.vote1 = request.POST.get("vote_1")
                        resultvote.vote2 = request.POST.get("vote_2")
                        resultvote.vote3 = request.POST.get("vote_3")
                        resultvote.vote4 = request.POST.get("vote_4")
                        resultvote.vote5 = request.POST.get("vote_5")
                        resultvote.voted = True
                        resultvote.comment = request.POST.get("comment")            
                        average_vote = ( int(request.POST.get("vote_1")) + int(request.POST.get("vote_2")) \
                                     + int(request.POST.get("vote_3")) + int(request.POST.get("vote_4")) \
                                     + int(request.POST.get("vote_5")) ) / 5
                        resultvote.average_vote = average_vote                    
                        resultvote.save()
                    
                        checkAmounts(challenge_pk)
                        
                        juror_votes = ResultVote.objects.filter(juror = juror, voted = True)                        
                        juror.average_vote = 0
                        for v in juror_votes:
                            juror.average_vote += v.average_vote
                        juror.average_vote = juror.average_vote / juror_votes.count()
                        juror.amount += 1
                        juror.save()
                            
                        response = {'status': 1}

    response = json.dumps(response, ensure_ascii=False)
    return HttpResponse(response, content_type="application/json; encoding=utf-8")

# Добавить информацию о лайке
@csrf_exempt
def add_like(request):
    
    if front.views.CUP_DATES['rating_change_finish'] and \
        front.views.CUP_DATES['rating_change_finish'] < timezone.now():
            response['error'] = 'Конкурс окончен'
    else:
        response = {'status': 0}
        type = request.POST.get('type')
        user_id = request.POST.get('user_id')
        count = request.POST.get('count')
        url = request.POST.get('url')
        
        if not type or not url:
            response['error'] = 'Отсутствуют обязательные параметры запроса'
        else:
            if (type == 'facebook' and not user_id) or (type == 'vk' and not count):
                response['error'] = 'Отсутствуют обязательные параметры запроса'
        
        if 'error' not in response:        
            try:
                challenge_id = int(urllib.parse.urlparse(url).path.split('/')[2])
                challenge = Challenge.objects.get(id = challenge_id)
            except:
                response['error'] = 'Не могу получить соревнование'    
            else:
                result = challenge.result_set.all().last()
                if not result:
                    response['error'] = 'Результат не существует'
                else:
                    if type == 'facebook':
                        resultlikehistory = ResultLikeHistory.objects.filter(result = result, user = user_id).first()
                        if resultlikehistory:
                            response['error'] = 'Повторное голосование невозможно'
                        else:                        
                            count = 1
                    else:                    
                        count = int(count) - ResultLikeHistory.objects.filter(result = result).count()
                        user_id = 'vk'
                                    
                    if count and count > 0:
                        resultlikehistory = ResultLikeHistory.objects.create(result = result, user = user_id).save()
                            
                        result.likes += 1
                        result.save()
                        
                        challenge.member.likes += 1
                        challenge.member.save()
                    
                        response = {'status': 1}
                    
    response = json.dumps(response, ensure_ascii=False)
    return HttpResponse(response, content_type="application/json; encoding=utf-8")    
