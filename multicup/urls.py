from .views import *
from django.contrib import admin
from django.contrib.staticfiles import views
from django.conf import settings
from django.conf.urls import include, url
from multicup.settings import ADMIN_PATH

urlpatterns = [
    url(r'^tasks/', include('tasks.urls')),
    url(r'^challenges/', include('challenges.urls')),
    url(r'^results/', include('results.urls')),
    url(r'^members/', include('members.urls')),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^admin/restore-default/$', restore_default),
    url(r'^' + ADMIN_PATH, admin.site.urls),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^', include('front.urls')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^test-email-template/', test_email),

]

if settings.DEBUG:
    urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve),
    ]
