from challenges.views import check_active_challenge
from challenges.models import Challenge
from datetime import datetime
from django.core.mail import send_mail
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from django.shortcuts import render
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils import timezone
from django_cron import CronJobBase, Schedule
from front.models import Content
from members.models import Member, Juror, MemberAmountHistory
from math import log
from multicup import settings
from results.models import Result, ResultLikeHistory
from tasks.models import Task

@staff_member_required
def restore_default(request):
    context = {}
    if request.method == 'POST':
        if request.POST.get('jurors'):
            jurors_user_id_list = [x for x in Juror.objects.all().values_list('member__user__id', flat = True)]
            User.objects.filter(id__in = jurors_user_id_list).delete()
            
        if request.POST.get('members'):            
            jurors_member_id_list = [x for x in Juror.objects.all().values_list('member__id', flat = True)]
            members_user_id_list = [x for x in Member.objects.exclude(id__in = jurors_member_id_list).values_list('user__id', flat = True)]
            User.objects.filter(id__in = members_user_id_list).delete()
        else:
            Challenge.objects.all().delete()
            MemberAmountHistory.objects.all().delete()
            Member.objects.all().update(
                amount = settings.BASE_AMOUNT,                
                rating = 0,
                likes = 0,
                challenge_completed = 0
            )
                
        if request.POST.get('tasks') :        
            Task.objects.all().delete()
        
        messages.success(request, 'База успешно очищена')
    return render(request, 'admin/restore-default.html', dict(context))



def test_email(request):
    context = {
        'title' : 'Ваш материал опубликован на сайте конкурса',
        #'content' : "Содержание письма",
        'server_url' : settings.SERVER_URL,
        'challenge_id' : 1,
        'comment' : 'Ужас',
        'title': 'Ваш конкурсный материал отклонен модератором'
        }
    #return render(request, 'email/email.html', dict(context))
    #return render(request, 'email/challenge-accept.html', dict(context))
    #return render(request, 'email/challenge-decline.html', dict(context))
    #return render(request, 'email/challenge-deadline.html', dict(context))
    return render(request, 'email/materials-for-vote.html', dict(context))
