import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Application definition

INSTALLED_APPS = [
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',    
    'ckeditor',
    'ckeditor_uploader',
    'challenges.apps.ChallengesConfig',
    'members.apps.MembersConfig',
    'tasks.apps.TasksConfig',
    'results.apps.ResultsConfig',
    'front.apps.FrontConfig',    
    'social.apps.django_app.default',
    'multiselectfield',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'multicup.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, "templates")],
        'APP_DIRS': False,        
        'OPTIONS': {
            'context_processors': [                
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social.apps.django_app.context_processors.backends',
                'social.apps.django_app.context_processors.login_redirect',
            ],
            'loaders': [                
                'admin_tools.template_loaders.Loader',
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader'
            ],            
        },        
    },
]

WSGI_APPLICATION = 'multicup.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'ru-RU'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = 'static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder', 
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

DEFAULT_CHARSET = 'utf-8'

AUTHENTICATION_BACKENDS = (
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.vk.VKOAuth2',    
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_VK_OAUTH2_KEY = '5681154'
SOCIAL_AUTH_VK_OAUTH2_SECRET = 'RGoxSe6kf5KiH61T1D5f'
SOCIAL_AUTH_VK_OAUTH2_SCOPE = ['email']

SOCIAL_AUTH_FACEBOOK_KEY = '207262666368953'
SOCIAL_AUTH_FACEBOOK_SECRET = '44298eef815a54bf4161cb463170b5b1'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
  'locale': 'ru_RU',
  'fields': 'id, name, email, age_range'
}

VK_LIKE_API_URL = 'https://api.vk.com/method/likes.getList'
FB_GRAPH_API_URL = 'https://graph.facebook.com/'

SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/members/after-login/'

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    'social.pipeline.user.create_user',
    'members.views.linkSocialUserToMember', 
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
)

CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
CKEDITOR_UPLOAD_PATH = "uploads/"

# Настройки Multicup
MULTICUP = {
    'JUROR_FOR_VOTE': 1,
    'NOTIFICATIONS': {        
        'CHALLENGE_CREATE'  : True, # Уведомлять Модератора, когда Соревнование сдано пользователем
        'CHALLENGE_ACCEPT'  : True, # Уведомлять Пользователя, когда Соревнование принято Модератором
        'CHALLENGE_DECLINE' : True, # Уведомлять Пользователя, когда Соревнование отклонено Модератором
        'CHALLENGE_EXTRA'   : True, # Уведомлять Пользователя, когда Соревнование продлено Модератором
        'CHALLENGE_CONTEST' : True, # Уведомлять Модератора, когда пользователь оспаривает соревнование
        'CHALLENGE_VOTE'    : True, # Уведомлять Жюри о необходимости оценки
        'CHALLENGE_EXPIRE'   : True, # Пользователя об истечении времени на задание
    },        
}

MATERIAL_UPLOAD_FILE_TYPES = ['image']
MATERIAL_UPLOAD_FILE_MAX_SIZE = 1024*1024*3

ADMIN_TOOLS_INDEX_DASHBOARD = 'multicup.dashboard.CustomIndexDashboard'

try:
    from multicup.local_settings import *
except ImportError:
    pass