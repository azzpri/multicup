/*  Удаление класса у элемента DOM
 *      cls - имя класса
 */
var test;
HTMLElement.prototype.removeClass = function(cls) {

    var classes = this.className.split(' ');

    for (var i = 0; i < classes.length; i++) {
        if (classes[i] == cls) {
            classes.splice(i, 1);
            i--;
        }
    }
    this.className = classes.join(' ');
}

/*  Проверка наличия класса у элемента DOM
 *      cls - имя класса
 */
HTMLElement.prototype.hasClass = function (cls) {
    return new RegExp('(\\s|^)' + cls + '(\\s|$)').test(this.className);
}

/*  Добавление класса элементу DOM
 *      cls - имя класса
 */
HTMLElement.prototype.addClass = function (cls) {
    if (! this.hasClass(cls)) {
        this.className += " " + cls;
    }    
}
    
/* Управления слайдером
    selector - указатель на DOM слайдера или сам элемент    
    ajax_url - url для инициализации через ajax
    callback - функция после окончания работы Slider
*/
function Slider(selector, ajax_url, callback) {
    
    function animate(draw, duration) {
        var start = performance.now();
      
        requestAnimationFrame(function animate(time) {
          // определить, сколько прошло времени с начала анимации
          var timePassed = time - start;
      
          // возможно небольшое превышение времени, в этом случае зафиксировать конец
          if (timePassed > duration) timePassed = duration;
      
          // нарисовать состояние анимации в момент timePassed
          draw(timePassed);
      
          // если время анимации не закончилось - запланировать ещё кадр
          if (timePassed < duration) {
            requestAnimationFrame(animate);
          }
      
        });
    }
    /*  Анимация слайда
    *  @param direction - направление сдвига (1 - вправо, -1 - влево)
    */
    function slideAnimation(direction) {
                        
        animate(function(timePassed) {            
            if (o.index == 0 && direction == -1) {
                o.dom.content.style.left = -(o.animation.range-timePassed)*3 + 'px';
            } else {                
                o.dom.content.style.left = -direction*3*timePassed - o.animation.range*(o.index-direction*3) + 'px';                
            }
        }, o.animation.range);
    }
    
    var o = {}
    o.dom = {}
    
    if (typeof selector === 'string')
        o.dom.slider = document.querySelector(selector);
    else
        o.dom.slider = selector;
        
    o.index = 0;
    o.slide_right_count = 0;
    o.ajax = '/ajax/slider-content/';
        
    // Инициализация слайдера через AJAX
    if (typeof ajax_url !== 'undefined') {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", ajax_url, true);
        xhr.send();
        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) return;
            if (xhr.status == 200) {            
                var data = JSON.parse(xhr.responseText);                  
                if (data['result'] !== 'undefined') {
                    var parent = o.dom.slider.parentElement;
                    o.dom.slider.outerHTML = data['result'];
                    o.dom.slider = parent.querySelector('.slider');
                    __init(o);  
                }
            }
        }
    } else {
      __init(o);      
    }

    // Инициализация слайдера
    function __init (o) {
                
        o.dom.content = o.dom.slider.querySelector('.slider-content');
        o.dom.left = o.dom.slider.querySelector('.arrow-left');
        o.dom.right = o.dom.slider.querySelector('.arrow-right');        
        o.data_length = parseInt(o.dom.content.getAttribute('data-length'));
          
        if (o.data_length > 3) {
            o.animation = {
                is_active : false,
                range : 290
            }
    
            o.dom.right.style.visibility = 'visible';
            o.name = o.dom.content.getAttribute('slider');
            o.div_length = o.dom.content.childElementCount;
          
            o.dom.right.onclick = function () {            
                if (o.index + 3 >= o.data_length)
                    return;
                
                o.index += 3;
                            
                // плавная анимация слайда
                if (!o.animation.is_active) {
                    slideAnimation(1, o.index);                
                }            
                            
                // показать левую кнопку
                if (o.index == 3) {
                    o.dom.left.style.visibility = "visible";
                }
                
                // если это последний элемент, убрать правую кнопку            
                if (o.index + 3 > o.data_length) {
                    
                    this.style.visibility = "hidden";
                }
                
                // если это не последний элемент, и далее ничего не прорисовано, рисуем еще 1 элемент
                if (o.slide_right_count < 1) {
                    o.slide_right_count ++;                
                } else {                  
                    if (o.data_length > o.div_length) {
                        var xhr = new XMLHttpRequest();
                        xhr.open("POST", o.ajax, true);
                        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
                        var body = 'slider=' + encodeURIComponent(o.name) +
                            '&start=' + encodeURIComponent(o.index + 3) +
                            '&end=' + encodeURIComponent(o.index + 9);
      
                        xhr.send(body);
                        xhr.onreadystatechange = function() {
                        if (xhr.readyState != 4) return;
                            if (xhr.status == 200) {
                                var response = JSON.parse(xhr.responseText);
                                if (typeof response["length"] !== 'undefined') {
                                    if (response["length"] > 0) {
                                        var content = o.dom.content.innerHTML;
                                        o.dom.content.innerHTML += response.result;
                                        o.div_length = o.dom.content.childElementCount;
                                        o.dom.content.style.width = o.div_length*300 + "px";
                                    }
                                }
                            }
                            if (typeof callback !== 'undefined')
                                callback(o);
                        }
                    }
                    o.slide_right_count = 0;  
                }
                
            }
            
            o.dom.left.onclick = function () {
                if (typeof o.index - 3 < 0)
                    return;
                
                // если это был последний элемент, показываем кнопку вправо
                if (o.index + 3 > o.data_length) {
                    o.dom.right.style.visibility = "visible";
                }
                
                // если это стал первый элемент, убираем кнопку влево
                if (o.index - 3 <= 0) {
                    this.style.visibility = "hidden";
                }
                            
                o.index -= 3;
                if (!o.animation.is_active) {
                    slideAnimation(-1, o.index);                
                }
                                        
            }
        } else {
            if (typeof callback !== 'undefined')
                callback(o);
        }        
    }
}

/*  Управления формой логина
 */
var activeLoginButton = {
    button : false,
    hideLoginForm : function (e) {
        if (! activeLoginButton.button.dom.button.contains(e.target) && activeLoginButton.button.dom.form.style.display == "block") {
            activeLoginButton.button.dom.form.style.display = "none";
            window.removeEventListener('click', activeLoginButton.hideLoginForm);
            activeLoginButton.button = false;
        }    
    }
}

class LoginButton {
    constructor (selector) {
        this.dom = {
            button : document.querySelector(selector),
            form   : document.querySelector('header > .login-form')            
        }
        
        var _o = this;
        
        if (! this.dom.button || ! this.dom.form)
            return;
        
        this.dom.button.onclick = function () {            
            var top = window.pageYOffset || document.documentElement.scrollTop;
            _o.dom.form.style.left = (
                _o.dom.button.getBoundingClientRect().left + _o.dom.button.offsetWidth/2 - 150) + "px";
            _o.dom.form.style.top = (_o.dom.button.getBoundingClientRect().bottom + top + 37) + "px";
            _o.dom.form.style.display = 'block';
                        
            if (! activeLoginButton.button)
                window.addEventListener('click', activeLoginButton.hideLoginForm);
            
            activeLoginButton.button = _o;    
        }
    }
}
new LoginButton('header > .grid > .links > .login');

/*  Выполнение функции по имени "MyTasks.activate"
 */
function executeFunctionByName(functionName, context /*, args */) {
    var args = [].slice.call(arguments).splice(2);
    var namespaces = functionName.split(".");
    var func = namespaces.pop();
    for(var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]]; 
    }
    return context[func].apply(context, args);
}

/*  Получение cookie
 */
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


/*
*      Класс попапа
*/
class Popup {
    constructor (attrs) {

        this.dom = {
           popup : document.querySelector(attrs.selector)  
        }
        this.content = {}
        
        // Контекст - это то, что определяет поп-ап при вызове.
        // Например, для поп-апа задачи контекстом будет являться task_id
        this.context;       
    
        if (! this.dom.popup)
            return;
    
        var popup = this;
        var popup_contents_selector = this.dom.popup.querySelectorAll('.popup-content-block');
    
        if (popup_contents_selector) 
            [].forEach.call(popup_contents_selector, function (e) {
                var content_name = e.getAttribute('popup-content-name');
                var content = new popupContent(content_name, e, popup);
                popup.content[content_name] = content;                
            });
            
        [].forEach.call(popup.dom.popup.querySelectorAll('.close-popup'), function (e) {        
            e.onclick = function () {                
                popup.hide();
            }    
        });      
           
        this.dom.popup.onclick = function (e) {
            popup.hide();
        };
        
        [].forEach.call(popup.dom.popup.querySelectorAll('.popup-content-block'), function (e) {
            e.onclick = function (event) {
                event.stopPropagation();                    
            }
        })         
    }

    /*  Прячем поп-ап
     */
    hide () {        
        if (typeof this.context !== 'undefined') {
            if (typeof this.context.return_to !== 'undefined') {
                this.show(this.context.return_to);
                return;
            }
            if (typeof this.context.confirm !== 'undefined') {
                var content_name = this.context.content_name;
                popup.alert(this.context.confirm, undefined, function () {
                    popup.show(content_name);
                });                
                //if (confirm(this.context.confirm))
                //    this.dom.popup.style.display = 'none';
                return;                
            }
        }
        this.dom.popup.style.display = 'none';
    }
    
    /*  Показываем попап
    *      content_name - название попапа
    *      attrs - атрибуты для данного контента
    */              
    show (content_name, attrs, callback) {    
        if (typeof this.content[content_name] === 'undefined')
            return;
        
        this.context = attrs;
        if (typeof attrs === 'undefined')
            this.context = {'content_name' : content_name};
        else
            this.context.content_name = content_name;
            
        this.dom.popup.style.display = 'block';
        
        for (var _content_name in this.content) {                
            this.content[_content_name].hide();
        }
        
        this.content[content_name].show(attrs, callback);
        window.scrollTo(0,0);
    };
    
    /*  Переопределить высоту страницы
     */
    redraw () {
        setTimeout( function (){
            var body_height = document.querySelector('body').offsetHeight;
            popup.dom.popup.style.height = 'auto';
            var popup_height = popup.dom.popup.offsetHeight;
            
            if (body_height < popup_height)
                document.querySelector('.page-content').style.minHeight = (popup_height - 400) + "px";    
            
            popup.dom.popup.style.height = document.querySelector('body').offsetHeight + "px";
        }, 300);
    }
}

/*
*      Класс блока содержимого поп-апа
*/
class popupContent {
    constructor (name, element, popup) {
        this.dom = {
            content : element,
        }
        this.name = name;
        this._popup = popup;
    }

    /*  Показываем блок попапа
     *      attrs - параметры для функции callback
     *      callback
    */
    show (attrs, callback) {
        if (typeof callback !== 'undefined')
            callback(this, attrs); 
        this.dom.content.style.display = 'block';
        this._popup.redraw();
    }

    /*  Скрываем блок попапа         
    */
    hide () {
       this.dom.content.style.display = 'none';
    }
}

var popup = new Popup({
    selector : '.popup',
});

/*  При клике на материал показываем его в попапе
 *      element - указатель на элемент, где искать
 *      return_to - какой контент показать в случае попытки закрытия поп-апа
*/
popup.addOnClickToMaterials = function (element, return_to) {    
    if (typeof popup.content.material === 'undefined')
        return;
        
    if (typeof element === 'undefined')
        element = document;
        
    [].forEach.call(element.querySelectorAll('.slider-content > div.material'), function (material) {        
        material.onclick = function () {            
            var challenge_id = material.getAttribute('challenge-id');
            if (challenge_id) {                
                var search_challenge_id = /materials([^&]+)/.exec(window.location.pathname);            
                if (search_challenge_id && search_challenge_id.length > 0) {
                    window.location = '/materials/' + challenge_id;
                } else {    
                    if (typeof return_to !== 'undefined')
                        popup.showMaterial(challenge_id, return_to);
                    else
                        popup.showMaterial(challenge_id);
                }
            }
        }        
    });
}

/*  Ввод и отправка комментария на сервер
 *      attrs {
            selector - указатель на DOM иои он сам
            ajax : {
                form : - указатель на DOM формы или она сама
                url : - адрес для отправки
            }
            min_length - минимальная длина поля (optional)
        }
 */
class AddCommentBox {
    constructor (attrs) {        
        for (var key in attrs) {            
            if (typeof this[key] === 'undefined')
                this[key] = attrs[key];
        }
        
        if (typeof attrs.selector === 'string')
            this.box = querySelector(attrs.selector)
        else this.box = attrs.selector;
        
        if (typeof this.min_length === 'undefined')
            this.min_length = 20;
        
        if (typeof this.ajax === 'undefined')
            this.ajax = false;
        
        if (typeof this.afterSuccessAjaxLoad === 'undefined')    
            this.afterSuccessAjaxLoad = function (data) {}        
        
        this.textarea = this.box.querySelector('textarea');
        this.button = this.box.querySelector('.button');
        this.error = this.box.querySelector('.error');
        this.loading = this.box.querySelector('.loading');
        
        var _this = this;
        
        this.button.onclick = function () {
            if (_this.textarea.value.length < _this.min_length) {
                _this.textarea.style.border = "1px solid #fb7066";
                _this.error.style.visibility = 'visible';   
            } else {
                _this.error.style.visibility = 'hidden';
                _this.button.style.display = "none";
                _this.loading.style.display = "block";
                
                if (_this.ajax) {
                    if (typeof _this.ajax.form === 'string')
                        _this.form = querySelector(_this.ajax.form)
                    else _this.form = _this.ajax.form;
                    
                    var data = new FormData(_this.form);
                    var xhr = new XMLHttpRequest();                    
                    xhr.open("POST", _this.ajax.url, true);
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                    xhr.send(data);
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState != 4) return;
                        if (xhr.status == 200) {                            
                            var data = JSON.parse(xhr.responseText);
                            _this.button.style.display = "block";
                            _this.loading.style.display = "none";
                            if (typeof data.status === 'undefined' || data.status == 0) {
                                var error = "При отправки данных на сервер произошла ошибка"
                                if (typeof data.error !== 'undefined')
                                    error = data.error;                                                    
                                popup.alert(error, 'material');
                            } else {
                                _this.box.style.display = 'none';
                                _this.textarea.value = "";
                                _this.afterSuccessAjaxLoad(data);
                            }
                        }
                    }   
                }
            }
        }
        
        this.textarea.onblur = function () {
            if (this.value.length > 0) {
                this.style.border = "solid 1px rgba(0, 0, 0, 0.2)";
                _this.error.style.visibility = 'hidden';
            } 
        }
        popup.redraw();
        return _this;
    }

}

/*  Показать поп-ап
 *      message - сообщение
 *      return_to - возврат к поп-апу
 *      confirm - функция отмены подтверждения
 */
popup.alert = function (message, return_to, confirm) {
    if (typeof popup.content.alert === 'undefined')
        return;
    
    attributes = {}
    if (typeof return_to !== 'undefined')
        attributes.return_to = return_to;
    
    popup.content.alert.dom.content.querySelector('.message').innerHTML = message;
    popup.content.alert.dom.content.querySelector('.button.ok').onclick = function () {
        popup.hide();
    }
    if (typeof confirm !== 'undefined') {
        popup.content.alert.dom.content.querySelector('.cancel').style.display = 'inline-block';
        popup.content.alert.dom.content.querySelector('.cancel').onclick = confirm;
    } else {
        popup.content.alert.dom.content.querySelector('.cancel').style.display = 'none';
    }
    popup.show('alert', attributes, function (content, attrs) {
        popup.redraw();        
    });
}

if (typeof popup.content.material !== 'undefined') {
    
    popup.addOnClickToMaterials();
    
    if (document.querySelector('.popup .popup-material-card .card-wrap .share')) {        
        document.querySelector('.vk-open-api').onload = function () {
            VK.init({apiId: 5681154, onlyWidgets: true});
            VK.Widgets.Like("vk-like", {type: "button", height: 30});            
        }                
    }
    
    // оценка жюри
    var box = popup.dom.popup.querySelector('.juror-vote-box');
    if (box){
        
        var comment_box = popup.dom.popup.querySelector('.juror-vote-form .add-comment-box');

        [].forEach.call(box.querySelectorAll('.votes .vote .numbers span'), function (number) {
            number.onclick = function () {
                var previous = number.parentNode.querySelector('.selected');
                if (previous)
                    previous.removeClass('selected');
                number.addClass('selected');
                number.parentNode.querySelector('input').value = number.innerHTML;
                number.parentNode.parentNode.querySelector('.title').style.color = "#4c4c4c";
                
                var inputs = box.querySelectorAll('.votes .numbers input');
                var inputs_count = inputs.length;
                var sum = 0;
                var count = 0;                            
                for (var i = 0; i < inputs_count; i ++) {
                    if ( inputs[i].value.length > 0) {
                        sum += parseInt(inputs[i].value);
                        count ++;
                    }
                }
                if (count == 5 && comment_box.querySelector('textarea').value.length > 0)
                    comment_box.querySelector('.error').style.visibility = 'hidden';
                
                var result = (sum/count).toFixed(1);
                box.querySelector('.numbers.average').innerHTML = result;
                box.querySelector('.numbers.average').parentNode.querySelector('input').value = result;
            }    
        });
        
        comment_box.querySelector('textarea').onblur = function () {
            if (this.value.length > 0) {
                this.style.border = 'solid 1px rgba(0, 0, 0, 0.2)';
                
                var count = 0;
                [].forEach.call(box.querySelectorAll('.votes .numbers input'), function (input) {
                    if (input.value.length > 0)
                        count ++;
                });
                if (count == 5)
                    comment_box.querySelector('.error').style.visibility = 'hidden';
            }
        }
        
        comment_box.querySelector('.button').style.display = "block";
        comment_box.querySelector('.loading').style.display = "none";
                
        test = comment_box.querySelector('.button'); 
        comment_box.querySelector('.button').onclick = function () {        
            var error = false;
            
            if (comment_box.querySelector('textarea').value.length == 0) {
                error = 'Вам нужно оставить комментарий';
                comment_box.querySelector('textarea').style.border = "1px solid #fb7066";
            }
                
            var inputs = box.querySelectorAll('.votes .numbers input');
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].value.length == 0) {
                    error = 'Необходимо поставить все оценки';
                    inputs[i].parentNode.parentNode.querySelector('.title').style.color = "#fb7066";
                }
            }            
            if (error) {             
                comment_box.querySelector('.error').innerHTML = error;
                comment_box.querySelector('.error').style.visibility = 'visible';
            } else {                
                comment_box.querySelector('.error').style.visibility = 'hidden';
                comment_box.querySelector('.button').style.display = "none";
                comment_box.querySelector('.loading').style.display = "block";
                
                var form = comment_box.closest('form');
                var data = new FormData(form);
                var xhr = new XMLHttpRequest();
                xhr.open("POST", '/results/vote/' + popup.context.challenge_id, true);
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                xhr.send(data);
                xhr.onreadystatechange = function() {
                    if (xhr.readyState != 4) return;                            
                    if (xhr.status == 200) {                        
                        var data = JSON.parse(xhr.responseText);
                        if (typeof data.status === 'undefined' || data.status == 0) {
                            var error = "При отправки данных на сервер произошла ошибка"
                            if (typeof data.error !== 'undefined')
                                error = data.error;
                            comment_box.querySelector('.button').style.display = "block";
                            comment_box.querySelector('.loading').style.display = "none";    
                            popup.alert(error, 'material');
                        } else {
                            popup.hide();
                        }
                    }
                }
            }
        }        
    }
    
    /*  Показать материал в попапе
     *      challenge_id - идентификатор соревнования
     *      return_to - куда вернуться в случае закрытия поп-апа
     */
    popup.showMaterial = function (challenge_id, return_to) {
        var context = {'challenge_id': challenge_id}
        if (typeof return_to !== 'undefined')
            context.return_to = return_to;
        popup.show('material', context, function (content, attrs) {
            var _d = content.dom.content;
            if (_d.querySelector('.juror-comment')) {
                _d.querySelector('.juror-comment').style.display = 'none';
                _d.querySelector('.moderator-comment').style.display = 'none';
                _d.querySelector('.disput').style.display = 'none';
            }
            
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "/challenges/view/" + attrs.challenge_id, true);
            xhr.send();
            xhr.onreadystatechange = function() {
                if (xhr.readyState != 4) return;
                if (xhr.status == 200) {            
                    var data = JSON.parse(xhr.responseText);                    
                    
                    [].forEach.call(_d.querySelectorAll('.juror-vote-box, .add-comment-box'), function (box) {
                        box.style.display = 'none';
                    });
                        
                    if (typeof data.error !== 'undefined') {
                        console.log(data.error);
                    } else {                        
                        var share = document.querySelector('.popup .popup-material-card .card-wrap .share');                        
                        if (share) {
                            if (data.challenge.status == 'accepted') {
                                var dataHref = "http://" + window.location.hostname + "/materials/" + attrs.challenge_id;                            
                                var image;
                                if (typeof data.result !== 'undefined' && typeof data.result.image == 'string')
                                    image = "http://" + window.location.hostname + "/media/" + data.result.image;
                                //document.querySelector('meta.og-image').setAttribute('content', image);                            
                                share.querySelector('.fb-like').setAttribute('data-href', dataHref);
                                share.querySelector('.vk-share').innerHTML =
                                    VK.Share.button({
                                        url             : dataHref,
                                        title           : 'Кубок мультимедийного вызова',
                                        description     : 'Я участвую в конкурсе для авторов и редакторов "Кубок мультимедийного вызова"! Оцени мой материал и присоединяйся к конкурсу. Выведем наш регион в топ!',
                                        image           : image || "",
                                        noparse         : true
                                    }, {type: "custom", text: "<img src=\"https://vk.com/images/share_32.png\" width=\"32\" height=\"32\" />"});
                                    
                                share.querySelector('.fb-share').onclick = function () {
                                    FB.ui({
                                      method    : 'share',
                                      display   : 'popup',
                                      href      : dataHref,
                                      quote     : 'Я участвую в конкурсе для авторов и редакторов "Кубок мультимедийного вызова"! Оцени мой материал и присоединяйся к конкурсу. Выведем наш регион в топ!',
                                    }, function(response){});
                                }                                
                                share.style.display = 'block';                                
                            } else {
                                share.style.display = 'none';
                            }
                        }
                        if (typeof data.result !== 'undefined') {
                            if (typeof data.result.image == 'string')
                                _d.querySelector('.material-info .image').style.backgroundImage = "url('/media/"+ data.result.image +"')";
                            _d.querySelector('.material-info .title').innerHTML = data.result.title;
                            _d.querySelector('.material-info .description').innerHTML = data.result.description.replace(new RegExp('\r?\n','g'), '<br />');
                            _d.querySelector('.material-info .link').innerHTML = data.result.url_title;
                            _d.querySelector('.material-info .link').setAttribute('href', data.result.url);
                            _d.querySelector('.member-info .likes span').innerHTML = data.result.likes;
                        } else {
                            _d.querySelector('.member-info .likes span').innerHTML = 0;
                        }
                            
                            if (typeof data.member.photo == 'string')
                                _d.querySelector('.member-info .avatar') .style.backgroundImage = "url('/media/"+ data.member.photo +"')";
                            _d.querySelector('.member-info .bio').innerHTML = data.member.bio;
                            _d.querySelector('.member-info .place').innerHTML = "№" + (parseInt(data.member.rating) + 1);
                            _d.querySelector('.member-info .amount span').innerHTML = data.challenge.total_amount;                                                        
                            _d.querySelector('.about-amount').style.display = "block";
                                                
                        if (_d.querySelector('.task-info')) {
                            _d.querySelector('.task-info .title').innerHTML = data.task.title;
                            _d.querySelector('.task-info .subtitle').innerHTML = data.task.format +
                                "&nbsp;" + data.task.cost + "&nbsp;" + data.task.duration;                            
                            if (typeof data.task.full_description !== 'undefined')
                                _d.querySelector('.task-info .description').innerHTML = data.task.full_description.replace(new RegExp('\r?\n','g'), '<br />');
                            else
                                _d.querySelector('.task-info .description').innerHTML = data.task.short_description.replace(new RegExp('\r?\n','g'), '<br />');
                            if (typeof data.task.links !== 'undefined' && data.task.links.length > 0) {
                                _d.querySelector('.task-info .links').innerHTML = '';
                                [].forEach.call(data.task.links, function (link) {
                                    _d.querySelector('.task-info .links').innerHTML += link;
                                });
                                _d.querySelector('.task-info .links').style.display = 'block';
                            } else {
                                _d.querySelector('.task-info .links').style.display = 'none';
                            }
                        }
                        
                        if (_d.querySelector('.juror-vote-box') && typeof data.to_vote !== 'undefined')                            
                            [].forEach.call(_d.querySelectorAll('.juror-vote-box, .add-comment-box'), function (box) {
                                box.style.display = 'block';
                            });
                        else
                            [].forEach.call(_d.querySelectorAll('.juror-vote-box, .add-comment-box'), function (box) {
                                box.style.display = 'none';
                            });
                            
                        [].forEach.call(_d.querySelectorAll('.comment-box-wrap-cloned'), function (box) {
                            box.remove();
                        });
                        
                        if (typeof data.votes !== 'undefined' && data.votes.length > 0) {
                            var box = _d.querySelector('.comment-box-wrap');
                            [].forEach.call(data.votes, function (vote) {
                                var new_box = box.cloneNode(true);
                                new_box.addClass('comment-box-wrap-cloned');
                                box.parentNode.appendChild(new_box);
                                box.parentNode.insertBefore(new_box, box.parentNode.querySelector('.disput'));
                                new_box.parentNode.querySelector('.juror-comment').style.display = 'block';                                
                                if (typeof vote.juror__member__photo === 'string')
                                    new_box.querySelector('.avatar').style.backgroundImage = "url('/media/" + vote.juror__member__photo + "')";
                                new_box.querySelector('.bio').innerHTML =
                                    vote.juror__member__user__first_name + "&nbsp;" + vote.juror__member__user__last_name;
                                if (vote.juror__member__job.length > 0 || vote.juror__member__company.length > 0)
                                    new_box.querySelector('.job').innerHTML =
                                        ",&nbsp" + vote.juror__member__job + "&nbsp" + vote.juror__member__company;
                                new_box.querySelector('.vote span').innerHTML = vote.average_vote;
                                new_box.querySelector('.comment').innerHTML = vote.comment;                                
                                new_box.style.display = 'block';
                                new_box.parentNode.style.display = 'block';
                            });
                        }                        
                    }
                                        
                    if (typeof data.challenge !== 'undefined')
                        var moderate = _d.querySelector('.moderation');                    
                        if (typeof data.moderate !== 'undefined' && moderate) {
                            var accept = _d.querySelector('.moderation.accept');
                            var add_time = _d.querySelector('.moderation.add-time');
                            accept.style.display = 'none';
                            add_time.style.display = 'none';
                            switch (data.challenge.status) {
                                case 'wm':
                                    accept.style.display = 'block';
                                    accept.querySelector('.add-comment-box .button').style.display = 'none';
                                    accept.querySelector('.add-comment-box').style.display = 'block';
                                    accept.querySelector('.blue-paragraph-title').innerHTML = 'Почему отклонили';
                                    [].forEach.call(accept.querySelectorAll('.panel > div'), function (button) {
                                        button.onclick = function () {
                                            var action = button.getAttribute('class');
                                            var textarea = accept.querySelector('textarea');                                            
                                            if (action == 'decline' && textarea.value.length == 0) {
                                                textarea.style.border = '1px solid #e45c40';
                                                var error = accept.querySelector('.add-comment-box .error');
                                                error.innerHTML = 'Вам нужно оставить комментарий';
                                                error.style.visibility = 'visible';
                                            } else {
                                                var formData = new FormData();
                                                formData.append("action", action);
                                                formData.append("comment", textarea.value);
                                                var xhr = new XMLHttpRequest();
                                                xhr.open("POST", '/challenges/accept/' + attrs.challenge_id, true);
                                                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                                                xhr.send(formData);
                                                accept.querySelector('.add-comment-box .loading').style.display = 'block';
                                                xhr.onreadystatechange = function() {
                                                    if (xhr.readyState != 4) return;
                                                    if (xhr.status == 200) {
                                                        window.location.reload();
                                                    }
                                                }    
                                            }
                                        }
                                    });
                                    break;
                                case 'decline':
                                case 'wom':
                                    add_time.style.display = 'block';
                                    add_time.querySelector('.add').onclick = function () {
                                        var value = parseInt(add_time.querySelector('input').value);
                                        if (value && value > 0) {
                                            var formData = new FormData();
                                            formData.append("time", value);
                                            var xhr = new XMLHttpRequest();
                                            xhr.open("POST", '/challenges/add_time/' + attrs.challenge_id, true);
                                            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                                            xhr.send(formData);
                                            xhr.onreadystatechange = function() {
                                                if (xhr.readyState != 4) return;
                                                if (xhr.status == 200) {
                                                    var data = JSON.parse(xhr.responseText);
                                                    window.location.reload();
                                                }
                                            }
                                        } else {
                                            add_time.querySelector('.error').style.display = 'block';
                                            add_time.querySelector('input').style.border = '1px solid #e45c40';
                                        }
                                    }
                                    break;                                
                            }
                        }
                        if (typeof data.challenge.moderator_comment !== 'undefined' || data.challenge.status == "wom" || data.challenge.status == "decline" ) {
                            _d.querySelector('.error-msg').style.display = 'block';
                            if (data.challenge.status == "wom") {
                                _d.querySelector('.error-msg').innerHTML = 'Не успел по времени';
                            } else {                                
                                _d.querySelector('.error-msg').innerHTML = "Задание отклонено модератором";
                                var box = _d.querySelector('.comment-box-wrap');
                                var new_box = box.cloneNode(true);
                                new_box.addClass('comment-box-wrap-cloned');
                                box.parentNode.appendChild(new_box);
                                box.parentNode.insertBefore(new_box, box.parentNode.querySelector('.disput'));
                                new_box.parentNode.querySelector('.moderator-comment').style.display = 'block';
                                new_box.querySelector('.juror-info').style.display = 'none';
                                new_box.querySelector('.comment').innerHTML = data.challenge.moderator_comment || "Нет комментария";
                                new_box.parentNode.querySelector('.disput').style.display = "block";
                                new_box.style.display = 'block';
                                new_box.parentNode.style.display = 'block';
                                                                            
                                var comment_box = new AddCommentBox({
                                    selector    : new_box.parentNode.querySelector('.add-comment-box'),
                                    ajax        : {
                                        url     : '/challenges/contest/' + popup.context.challenge_id,
                                        form    : new_box.parentNode.querySelector('.add-comment-box').parentNode
                                    },
                                    afterSuccessAjaxLoad : function (data) {
                                        new_box.parentNode.querySelector('.disput').style.display = "none";
                                        popup.alert("Ваш комментарий отправлен модератору. Дальнейший ответ придет вам на почту.", 'material');
                                    }
                                });

                                new_box.parentNode.querySelector('.disput').onclick = function () {
                                    if (comment_box.box.style.display == "none")
                                        comment_box.box.style.display = "block"
                                    else
                                        comment_box.box.style.display = "none";
                                }             
                            }
                        } else {
                            _d.querySelector('.error-msg').style.display = 'none';                            
                        }
                    
                    popup.redraw();
                }
            }
        });
    }
    
    // разбираем параметр
    var search_challenge_id = /ch=([^&]+)/.exec(location.search);
    if (search_challenge_id)
        popup.showMaterial(search_challenge_id[1]);
}


// Обработка полей на редактируемой форме в поп-апе
if (document.querySelector('section.popup > .grid > .popup-content-block.edit-form')) {
    
    var editForm = document.querySelector('section.popup > .grid > .popup-content-block.edit-form');            
    //  Очистка имени файла при удалении или замене        
    editForm.querySelector('.delete-file').onclick = function (e) {
        editForm.querySelector('.photo > .file-description').style.display = 'inline-block';
        editForm.querySelector('.photo > .file-name').innerHTML = "";
        editForm.querySelector('.photo > .delete-file').style.display = 'none';
        editForm.querySelector('.upload').value = "";
    }
    
    
    // Показываем, что файл загружен
    editForm.querySelector('.upload').onchange = function (e) {                
        var fileName = e.target.value.split( '\\' ).pop();
       
        if (fileName) {
            editForm.querySelector('.photo > .file-description').style.display = 'none';
            editForm.querySelector('.photo > .file-name').innerHTML = "Выбран файл: " + fileName;
            editForm.querySelector('.photo > .delete-file').style.display = 'inline-block';
        }
    }       
    
    // Обработка кликов по input в форме сдачи материалов
    var inputs = editForm.querySelectorAll("input[type='text'], textarea");
    for (var i = 0; i < inputs.length; i ++) {
        inputs[i].onfocus = function () {
            if (this.value == this.getAttribute('alt'))
                this.value = '';
            this.style.color = 'black';                
        }
        inputs[i].onblur = function () {
            if (this.value == '')
                this.value = this.getAttribute('alt');                        
            if (this.value == this.getAttribute('alt'))
                this.style.color = 'gainsboro';
        }
    }
}
            
/*  Переключение табов
*/
(function () {
    var tabs = document.querySelectorAll('.tabs-control > .tab');
    
    if (tabs.length == 0)
        return;

    for (var i = 0; i < tabs.length; i ++) {
        tabs[i].onclick = function () {                
            var tab = this;                
            if (tab.hasClass('active')) {
                return;
            }
           
            //вызываем функцию активации таба
            if (tab.getAttribute('activate')) {                    
                executeFunctionByName(tab.getAttribute('activate'), window);
            }
           
            // Меняем классы .active
            document.querySelector('.tabs-control > .active').removeClass('active');
            tab.addClass('active');
           
            // Прячем контейнеры и показываем нужный
            var tab_containers = document.querySelectorAll('.tab-container');
            for (var j = 0; j < tab_containers.length; j ++) {
                tab_containers[j].style.display = 'none';
            }
            var tab_container_class = tab.getAttribute('tab-class');
                        
            document.querySelector('.tab-container.' + tab_container_class).style.display = "block";
        }            
    }       
})();

if (document.querySelector('.members-rating .table')){
    
    // Отображение данных в таблице
    function draw_table(data) {
        var table = document.querySelector('.members-rating .table');
        
        table.innerHTML = '';
        
        var limit = 9;
        if (table.getAttribute('no-limit'))
            limit = false;
        
        for (var i = 0; i < data.length; i ++) {
            
            if (data.name == username) {
                found_user_in_table = true;
            }
            
            if (i >= 0 && i <= 2) {
                var el = document.querySelector('.members-rating .top-3 > div:nth-child('+ (i + 1) +')');
                el.querySelector('.image').style.backgroundImage = "url('/media/"+ data[i].image +"')";
                el.querySelector('.bio').innerHTML = data[i].first_name + " " + data[i].last_name;
                el.querySelector('.bio').setAttribute('member-id', data[i].id);
                el.querySelector('.amount').innerHTML = data[i].amount + " " + data[i].amount_declension; 
                el.querySelector('.materials').innerHTML = data[i].materials + " " + data[i].materials_declension; 
                el.querySelector('.city').innerHTML = data[i].city;
            } else if ((limit && i <= limit) || ! limit) {
                add_row_to_table(table, i + 1, data[i]);
            } else {
                if (found_user_in_table) {
                    add_row_to_table(table, '...', {
                        "name"      : "",
                        "bio"       : "",
                        "amount"    : "",
                        "likes"     : "",
                        "materials" : "",
                        "city"      : "",
                    });
                    add_row_to_table(table, data[i].position + 1, data[i]);
                    break;
                }
            }
        }
        onclickBio();
    }
    
    // Функция добавления элемента к строке
    function add_element_to_row(row, class_name, value){        
        var el = document.createElement('div');
            el.className = class_name;
            if (class_name != 'photo')
                el.innerHTML = value;
            else
                el.style.backgroundImage = "url('/media/"+ value +"')";                            
            row.appendChild(el);
    }
    
    // Отображение рейтинга пользователей
    (function() {   
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/members/rating/", true);
        xhr.send();
        xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
            if (xhr.status == 200) {            
                tableData = JSON.parse(xhr.responseText);
                draw_table(tableData);
            }
        }
    }())
    
    // Функция добавления строки в таблицу
    function add_row_to_table(table, i, data) {
        var row = document.createElement('div');
        var name = data.first_name + " " + data.last_name;
        
        row.className = 'row';
        
        add_element_to_row(row, 'position', data.position + 1);
        
        if (table.getAttribute('no-limit'))            
            add_element_to_row(row, 'photo', data.image);
            
        add_element_to_row(row, 'bio', name);
        var divs = row.querySelectorAll('div');
        divs[divs.length - 1].setAttribute('member-id', data.id);
        
        if (i == 4) {
            add_element_to_row(row, 'amount', data.amount);
            add_element_to_row(row, 'amount_declension', data.amount_declension);
            add_element_to_row(row, 'likes', data.likes);
            add_element_to_row(row, 'likes_declension', data.likes_declension);
            add_element_to_row(row, 'materials', data.materials);
            add_element_to_row(row, 'materials_declension', data.materials_declension);
        } else {
            add_element_to_row(row, 'amount', data.amount);
            add_element_to_row(row, 'amount_declension', '');
            add_element_to_row(row, 'likes', data.likes);
            add_element_to_row(row, 'likes_declension', '');
            add_element_to_row(row, 'materials', data.materials);
            add_element_to_row(row, 'materials_declension', '');
        }
        add_element_to_row(row, 'city', data.city);
        
        if (name == username) {
            if (table.getAttribute('no-limit'))
                row.style.backgroundColor = 'rgba(163,204,255,0.3)';
            else {
                row.style.fontWeight = 600;
            }
        }
        
        table.appendChild(row);    
    }
    
    // Сортировка данных
    var filters = document.querySelectorAll('.members-rating-sort > div');
    
    function sort_table_data(field, direction) {
        tableData = tableData.sort(function(a, b){        
            var nameA = a[field], nameB = b[field];
            
            if (typeof nameA === 'string') {
                nameA = nameA.toLowerCase();
                nameB = nameB.toLowerCase();
            }
            
            if (direction == 'asc') {
                if (nameA < nameB) 
                    return -1;
                if (nameA > nameB){                                
                    return 1;
                }
            } else {
                if (nameA < nameB){                
                    return 1;
                }
                if (nameA > nameB)
                    return -1;
            }         
            return 0;
        });
        
        for (var i = 0; i < tableData.length; i++) {
            tableData[i].position = i;
        }
    }
            
    for (var i = 0; i < filters.length; i++) {
        filters[i].onclick = function () {        
            // что-то пошло не так, данных нет    
            if (! tableData){
                console.log('Нет данных tableData');
                return;
            }
                    
            var selected = document.querySelector('.members-rating-sort > .selected');
            selected.querySelector('.up').style.display = 'none';
            selected.querySelector('.down').style.display = 'none';
            selected.removeClass('selected');
            
            var direction = 'asc';
            
            this.addClass('selected');
            if (this.hasClass('desc')) {
                this.removeClass('desc');
                this.addClass('asc');
                this.querySelector('span.up').style.display = 'inline-block';
                this.querySelector('span.down').style.display = 'none';
            } else {
                this.removeClass('asc');
                this.addClass('desc');
                direction = 'desc';
                this.querySelector('span.down').style.display = 'inline-block';
                this.querySelector('span.up').style.display = 'none';
            }
                    
            var sort_field = 'last_name';
                            
            if (this.hasClass('amount')) {
                sort_field = 'amount';
            }
            
            if (this.hasClass('materials')) {
                sort_field = 'materials';
            }
            
            if (this.hasClass('likes')) {
                sort_field = 'likes';
            }
                    
            sort_table_data(sort_field, direction); 
            draw_table(tableData);
        }
    }
    
    function onclickBio () {
        [].forEach.call(document.querySelectorAll('.members-rating .bio'), function (bio) {
            bio.onclick = function () {
                popup.viewProfile(bio.getAttribute('member-id'));
            }
        });    
    }
}

function viewProfile (profile_container) {
    var filters_group = profile_container.querySelector('.category-filters');    
    var slider_content = profile_container.querySelector('.slider-content');    
    if (filters_group) {
        if (slider_content && slider_content.querySelectorAll('.material').length > 0)
            filters_group.style.display = 'block';
        else
            filters_group.style.display = 'none';
        [].forEach.call(filters_group.querySelectorAll('.filter'), function (filter) {
            filter.onclick = function () {
                filters_group.querySelector('.activated').removeClass('activated');                    
                filter.addClass('activated');
                filters_group.querySelector('.active-category').innerHTML = filter.innerHTML;                
                var filter_values = filter.getAttribute('filter-value').split(',');                                
                var displayed = 0;                
                for (var i = 0; i < materials.length; i ++) {                    
                    var material = materials[i];
                    var status = material.getAttribute('status');
                    material.style.display = 'none';
                    for (var j in filter_values) {
                        if (status == filter_values[j]) {
                            if (displayed == 0 || displayed % 3 == 0)
                                material.style.margin = '0 0 8px 0';
                            else
                                material.style.margin = '0 0 8px 3px';
                            displayed ++;
                            material.style.display = 'inline-block';
                            break;
                        }                    
                    }                
                }
            }
        });
    }                
    if (slider_content) {
        var materials = slider_content.querySelectorAll('.material');
        var displayed = 0;
        for (var i = 0; i < materials.length; i ++) {                    
            var material = materials[i];
            var status = material.getAttribute('status');
            if (status != 'accepted' && status != 'voted') {                
                material.style.display = 'none';
            } else {                
                if (displayed == 0 || displayed % 3 == 0)
                    material.style.margin = '0 0 8px 0';
                else
                    material.style.margin = '0 0 8px 3px';
                displayed ++;
            }
        }
        popup.addOnClickToMaterials(profile_container, 'view-profile');        
    }    
}

if (typeof popup.content['view-profile'] !== 'undefined') {
    /*  Показать профиль участника в попапе
     *      member_id - идентификатор соревнования
     */
    popup.viewProfile = function (member_id) {
        popup.show('view-profile', {'member_id': member_id}, function (content, attrs) {            
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "/ajax/view-profile/" + attrs.member_id, true);
            xhr.send();
            xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) return;
                if (xhr.status == 200) {            
                    content.dom.content.innerHTML = xhr.responseText;
                    viewProfile(content.dom.content);
                }
            }
        });                    
    };
        
    
    var search_member_id = /member=([^&]+)/.exec(location.search);
        if (search_member_id)
            popup.viewProfile(search_member_id[1]);
}

/*  Очистка сообщения пользователя по нажатию на X
 */
(function () {
    var close_message = document.querySelector('section.message > .grid > .close');
    
    if (! close_message)
        return;
    
    close_message.onclick = function () {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/members/clear-message/", true);
        xhr.send();
        xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
            document.querySelector('section.message').style.display = 'none';
        }
    }
})();