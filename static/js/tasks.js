   
/*  Переключение фильтров
*/
var task_filter_params = {}
function filter_data(action, param, value) {
   
    if (action == 'add') {
        task_filter_params[param] = value;
    } else {
        if (typeof task_filter_params[param] !== 'undefined')
            delete task_filter_params[param];                                
    }
   
    var tasks = document.querySelectorAll('section.tasks > .grid > .tab-tasks > .tasks > .task');
   
    for (var i = 0; i < tasks.length; i ++) {
        if (Object.keys(task_filter_params).length == 0) {
            tasks[i].style.display = 'inline-block';
        } else {
            var show = false;                    
            for (var j in task_filter_params) {
                if (tasks[i].getAttribute(j) == task_filter_params[j]) {
                    show = true;                            
                } else {
                    show = false;
                    break;
                }
            }
            if (show) {
                tasks[i].style.display = 'inline-block';
            } else {
                tasks[i].style.display = 'none';
            }
        }
    }            
}

var filter_groups = document.querySelectorAll('section.tasks > .grid > .tab-tasks > .filters .filter-group');
       
for (var i = 0; i < filter_groups.length; i ++) {            
    var filters = filter_groups[i].querySelectorAll('.filter');
    
    for (var j = 0; j < filters.length; j++) {            
        filters[j].onclick = function () {
            var filter = this;
            if (filter.hasClass('active')){
                filter.removeClass('active');
                filter_data('remove', filter.getAttribute('filter-name'), filter.getAttribute('filter-value'));
            } else {
    
                // Меняем классы .active                                                
                if (filter.parentElement.querySelector('.active'))
                    filter.parentElement.querySelector('.active').removeClass('active');
                filter.addClass('active');
                filter_data('add', filter.getAttribute('filter-name'), filter.getAttribute('filter-value'));
            }
        }
    }
}


// отображение попапа при клике на задачи
[].forEach.call(
    document.querySelectorAll('section.tasks > .grid > .tab-tasks > .tasks > .task'),
    function (task) {        
        task.onclick = function () {  
            popup.show('task', {
                    task_id : this.getAttribute('task-id'),                    
                }, function (content, attrs) {                    
                    content.dom.content.querySelector('.title').innerHTML = task.querySelector('.title').innerHTML;
                    
                    var task_format_key = task.getAttribute('format');
                    var task_format = '';
                    var description = task.querySelector('.description').innerHTML;                                    
                    var format_elements = document.querySelectorAll('section.tasks > .grid > .tab-tasks > .filters > .formats > div');
                    [].forEach.call(
                        document.querySelectorAll('section.tasks > .grid > .tab-tasks > .filters > .formats > div'),
                        function (format) {
                            if (format.getAttribute('filter-value') == task_format_key)
                                task_format = format.innerHTML;                                                            
                    });
                    content.dom.content.querySelector('.subtitle').innerHTML = task_format + "&nbsp;&nbsp;" + description;
                    
                    content.dom.content.querySelector('.description').innerHTML = 'Загрузка ...';
                    
                    if (content.dom.content.querySelector('.how-it-made'))
                        content.dom.content.querySelector('.how-it-made').style.display = 'none';
                                                            
                    var buy = content.dom.content.querySelector('.buy');
                    if (buy)
                        buy.style.display = "none";
                        
                    var cannot_buy = content.dom.content.querySelector('.cannot_buy');
                    if (cannot_buy)
                        cannot_buy.style.display = "none";
                        
                    // подгружаем остальную информацию c сервера
                    var xhr = new XMLHttpRequest();
                    xhr.open("GET", "/tasks/view/" + attrs.task_id, true);
                    xhr.send();
                    
                    xhr.onreadystatechange = function() {
                        
                        if (xhr.readyState != 4) return;
                        
                        if (xhr.status == 200) {            
                            var data = JSON.parse(xhr.responseText);
                           
                            if (typeof data['task'] === 'undefined' || data['task'] == 0) {
                                content.dom.content.querySelector('.description').innerHTML = "Невозможно отобразить задачу!";
                                return;    
                            } else {
                                
                                if (typeof data.task.full_description !== 'undefined')
                                    content.dom.content.querySelector('.description').innerHTML = data.task.full_description.replace(new RegExp('\r?\n','g'), '<br />');
                                else
                                    content.dom.content.querySelector('.description').innerHTML = data.task.short_description.replace(new RegExp('\r?\n','g'), '<br />');
                                    
                                if (typeof data.task.links !== 'undefined' && data.task.links.length > 0) {                            
                                    var links_html = "";
                                    for (var i = 0; i < data.task.links.length; i ++){
                                        links_html += data.task.links[i];
                                    }
                                    content.dom.content.querySelector('.links').innerHTML = links_html;
                                    content.dom.content.querySelector('.how-we-do').style.visibility = 'visible';
                                }
                                
                                if (buy) {
                                    if (typeof User.amount === 'undefined') {
                                        buy.style.display = "block";                                        
                                    } else {
                                        if (data.task.cost > User.amount) {
                                                cannot_buy.style.display = "block";
                                        } else {
                                            buy.innerHTML = "Купить задание за "+ data.task.cost +" баллов";
                                            buy.style.display = "block";
                                        }
                                    }
                                }
                                                                
                                if (parseInt(data.task.completed) > 0 && typeof data.task.full_description === 'undefined') {
                                    Slider(content.dom.content.querySelector('.slider'), '/ajax/task-materials-slider/' + attrs.task_id,
                                        function () {
                                            popup.redraw();
                                            content.dom.content.querySelector('.how-it-made').style.display = 'block';
                                            content.dom.content.querySelector('.how-it-made .count').innerHTML = 'Уже сделали ' + data.task.completed + ' человек';
                                            popup.addOnClickToMaterials(content.dom.content.querySelector('.how-it-made'));
                                        });                                                                                                                                            
                                }
                                
                                if (typeof data.task.full_description !== 'undefined') {
                                    content.dom.content.querySelector('.not-checked').style.display = 'none';
                                    content.dom.content.querySelector('.checked').style.display = 'none';
                                    Slider(
                                        content.dom.content.querySelector('.not-checked .slider'),
                                        '/ajax/task-unchecked-slider/' + attrs.task_id,
                                        function (o) {
                                            if (o.dom.content.querySelector('div'))
                                                content.dom.content.querySelector('.not-checked').style.display = 'block';                                                                                        
                                            popup.redraw();
                                            popup.addOnClickToMaterials(content.dom.content.querySelector('.not-checked'));                                            
                                        }
                                    );
                                    Slider(
                                        content.dom.content.querySelector('.checked .slider'),
                                        '/ajax/task-materials-slider/' + attrs.task_id,
                                        function (o) {
                                            if (o.dom.content.querySelector('div'))
                                                content.dom.content.querySelector('.checked').style.display = 'block';
                                            popup.redraw();
                                            popup.addOnClickToMaterials(content.dom.content.querySelector('.checked'));
                                        }
                                    );
                                }
                            }
                        }
                    }
                }
            );        
        }
    }
    
);


if (document.querySelector('.tab-my-tasks')) {
    var MyTasks = function () {
    
        var o = {
            dom : {
                tab : document.querySelector('.tab-my-tasks'),
            }
        }
        o.dom.task = o.dom.tab.querySelector('.task');
        o.dom.title = o.dom.task.querySelector('.title');
        o.dom.subtitle = o.dom.task.querySelector('.subtitle');
        o.dom.description = o.dom.task.querySelector('.description');
        o.dom.links = o.dom.task.querySelector('.links');
        o.dom.button = o.dom.task.querySelector('.button');
        o.dom.completed = o.dom.task.querySelector('.completed');
        o.dom.link_under_button = o.dom.task.querySelector('.link-under-button');
        o.dom.error_msg = o.dom.task.querySelector('.error-msg:not(.success-msg)');
        o.dom.success_msg = o.dom.task.querySelector('.success-msg');
        o.challange_id;
        
        //  Получение данных с сервера и повторная активация
         
        var __getDataActivate = function () {
            
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "/tasks/user_task/", true);
            xhr.send();
            xhr.onreadystatechange = function() {
                if (xhr.readyState != 4) return;
                if (xhr.status == 200) {            
                    var data = JSON.parse(xhr.responseText);
                    o.activate(data);
                }
            }
        }
        
        /*  Активация закладки Мои задачи
         */
        o.activate = function (data) {                    
            if (typeof data === 'undefined'){
                // Проверяем, активировались ли уже задачи и если да, пропускаем этот шаг
                if (typeof o.dom.tab.getAttribute('activated') !== 'undefined') {
                    return __getDataActivate();
                }                        
            } else {
                if (data.task != 0) {                            
                    popup.challenge_id = data.challenge.id;
                    
                    // наполняем элементы данными
                    o.dom.title.innerHTML = data.task.title;
                    o.dom.subtitle.innerHTML = data.task.subtitle;
                    o.dom.description.innerHTML = data.task.full_description;
                    
                    if (typeof data.task.links !== 'undefined' && data.task.links.length > 0) {
                        var links_html = "";
                        for (var i = 0; i < data.task.links.length; i ++){
                            links_html += data.task.links[i]
                        }
                        o.dom.links.innerHTML = links_html;                            
                        o.dom.task.querySelector('.how-we-do').style.display = 'block';
                    }
                                        
                    if (data.challenge.status == 'initial' || data.challenge.status == 'extra') {
                        o.dom.button.style.display = 'block';
                        o.dom.error_msg.style.display = 'none';
                        if (typeof data.task.remained_time !== 'undefined' && o.dom.link_under_button)
                            o.dom.link_under_button.innerHTML = data.task.remained_time;                        
                    } else {
                        o.dom.button.style.display = 'none';
                        if (o.dom.link_under_button) {
                            o.dom.link_under_button.innerHTML = "Написать модераторам";
                            o.dom.link_under_button.addClass('clickable');
                            var add_comment_selector = o.dom.tab.querySelector('.add-comment-box');
                            var comment_box = new AddCommentBox({
                                selector    : add_comment_selector,
                                ajax        : {
                                    url     : '/challenges/contest/' + popup.challenge_id,
                                    form    : add_comment_selector.parentNode
                                },
                                afterSuccessAjaxLoad : function (data) {
                                    o.dom.link_under_button.style.display = 'none';
                                    popup.alert("Ваш комментарий отправлен модератору. Дальнейший ответ придет вам на почту.");
                                }
                            });

                            o.dom.link_under_button.onclick = function () {
                                if (! comment_box.box.offsetParent)
                                    comment_box.box.style.display = "block"
                                else
                                    comment_box.box.style.display = "none";
                            }                                
                        }
                        if (data.challenge.status == 'wm') {
                            o.dom.success_msg.style.display = 'block';                            
                        } else {
                            if (data.challenge.status == 'decline'){
                                o.dom.error_msg.innerHTML = "Материалы отклонены. ";
                                if (typeof data.task.decline_description !== 'undefined')
                                     o.dom.error_msg.innerHTML += data.task.decline_description;
                            } else {
                                o.dom.error_msg.innerHTML = "Вы не успели сдать материал к дедлайну";
                                if (data.task.member_lost > 0) {
                                    o.dom.error_msg.innerHTML += " и потеряли " + data.task.member_lost + " баллов";
                                } else {
                                    o.dom.error_msg.innerHTML += ". В первый раз баллы за задание списаны не будут, но впредь будте внимательнее.";
                                }    
                            }
                            o.dom.error_msg.style.display = 'block';
                        }
                    }                    
                    o.dom.completed.innerHTML = data.task.completed;                    
                } else {
                    o.dom.tab.querySelector('.task-card').innerHTML = '<div class="nothing-here">У вас еще нет активного задания, выберите его в разделе заданий</div>';
                    o.dom.tab.querySelector('.task-card').style.background = 'none';
                    o.dom.tab.querySelector('.task-card').style.textAlign = 'left';
                    o.dom.tab.querySelector('.task-card').style.padding = '0';
                }
                o.dom.tab.querySelector('.task-card').style.display = 'block';
                if (typeof data.published_materials !== 'undefined' && data.published_materials.length > 0) {
                    o.dom.tab.querySelector('.completed > .slider-content').innerHTML = data.published_materials;
                    popup.addOnClickToMaterials(o.dom.tab.querySelector('.completed > .slider-content'));
                } else {
                    o.dom.tab.querySelector('.completed > .slider-content').innerHTML = '<div class="nothing-here">У вас еще нет выполненных заданий</div>';
                }
                if (typeof data.published_materials !== 'undefined' && data.unpublished_materials.length > 0) {
                    o.dom.tab.querySelector('.unpublished > .slider-content').innerHTML = data.unpublished_materials;
                    popup.addOnClickToMaterials(o.dom.tab.querySelector('.unpublished > .slider-content'));
                } else {
                    o.dom.tab.querySelector('.unpublished > .slider-content').innerHTML = '<div class="nothing-here">У вас еще нет неопубликованных заданий</div>';
                }
                if (typeof data['member_amount'] !== 'undefined') {
                    document.querySelector('section.memberinfo > .grid .user-amount').innerHTML = data.member_amount;                            
                }
                // устанавливаем флаг активации
                o.dom.tab.setAttribute("activated", "");
                
                o.challange_id = data.challenge.id;
            } 
            // Просто показываем содержимое
            o.dom.tab.style.display = 'block';                                        
        }
        
        /*  Работа кнопки Сдать материалы
         */        
        o.dom.button.onclick = function () {
            popup.show('complete-task', {
                    challenge_id : o.challange_id
                }, function (content, attrs) {
                    
                }
            );                                    
        }
            
        return o;            
    }();
}

var base_js = document.querySelector('.base-js');
if (base_js)
    base_js.onload = function () {
                
        new LoginButton('.popup .card .button.login');
        
        if (typeof popup !== 'undefined') {
            // покупка задач или регистрация
            if (typeof popup.content.task !== 'undefined') {
            var buy = popup.dom.popup.querySelector('.card .buy');            
            if (buy && ! buy.hasClass('login'))
                buy.onclick = function () {                                        
                    if (typeof popup.context.task_id === 'undefined')
                        return;                       
                    var xhr = new XMLHttpRequest();
                    xhr.open("POST", '/challenges/add/' + popup.context.task_id, true);
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));                                                              
                    xhr.send();
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState != 4) return;                            
                        if (xhr.status == 200) {                        
                            var data = JSON.parse(xhr.responseText);
                            if (typeof data !== 'undefined' || typeof data['status'] === 'undefined') {
                                if ( data['status'] == 1 ) {
                                    if (typeof data.amount !== 'undefined')
                                        document.querySelector('section.memberinfo > .grid > .results > div .user-amount').innerHTML = data.amount;

                                        popup.hide();
                                   
                                        document.querySelector('section.tasks > .grid > .tabs-control > .active').removeClass('active');
                                        document.querySelector('section.tasks > .grid > .tabs-control > .tab:last-of-type').addClass('active');
                                        document.querySelector("div.task[task-id='"+ popup.context.task_id +"']").remove();
                                        document.querySelector('.tab-tasks').style.display = 'none';
                                        
                                        window.location = '/tasks/?my=1';
                                        //MyTasks.activate(data);
                                } else {
                                    popup.alert(data['error'], 'task');
                                }
                            } else {
                                console.log('При попытке взять задачу нет ответа от сервера!');
                            }
                        } else {
                            console.log('При попытке взять задачу произошла ошибка!');
                        }
                    }                
                }
            }
        }
        
        if (document.querySelector('.tab-my-tasks')) {
            
            var search_my_tab = /my=([^&]+)/.exec(location.search);
            if (search_my_tab)
                document.querySelector(".tabs-control > .tab[tab-class='tab-my-tasks']").click();
            
            // Сдача материалов
            popup.dom.popup.querySelector('.complete-task .button').onclick = function () {
                if (typeof popup.challenge_id === 'undefined')
                    return;
               
                // Валидация данных
                if (popup.dom.popup.querySelector('.complete-task .upload').files.length == 0
                ||  popup.dom.popup.querySelector('.complete-task input.title').value.length < 0
                ||  popup.dom.popup.querySelector('.complete-task input.title').value ==
                    popup.dom.popup.querySelector('.complete-task input.title').getAttribute('alt')
                ||  popup.dom.popup.querySelector('.complete-task input.link-title').value.length < 0
                ||  popup.dom.popup.querySelector('.complete-task input.link-title').value ==
                    popup.dom.popup.querySelector('.complete-task input.link-title').getAttribute('alt')
                ||  popup.dom.popup.querySelector('.complete-task input.link').value.length < 0
                ||  popup.dom.popup.querySelector('.complete-task input.link').value ==
                    popup.dom.popup.querySelector('.complete-task input.link').getAttribute('alt')
                ||  popup.dom.popup.querySelector('.complete-task textarea').value.length < 0
                ||  popup.dom.popup.querySelector('.complete-task textarea').value ==
                    popup.dom.popup.querySelector('.complete-task textarea').getAttribute('alt')
                ) {
                    popup.alert("Все поля обязательны для заполнения!", 'complete-task');
                    return;
                }
               
                //сериализация данных
                var form = popup.dom.popup.querySelector('.complete-task form');
                var data = new FormData(form);
                                       
                var xhr = new XMLHttpRequest();
               
                xhr.open("POST", '/challenges/complete/' + popup.challenge_id, true);
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
               
                document.querySelector('.complete-task .button').style.display = 'none';
                document.querySelector('.complete-task .loading').style.display = 'block';
               
                xhr.send(data);
                xhr.onreadystatechange = function() {
                    if (xhr.readyState != 4) return;
                    // если ответ положительный
                    if (xhr.status == 200) {         
                        var data = JSON.parse(xhr.responseText);
                        if (typeof data !== 'undefined' || typeof data['status'] === 'undefined') {
                            if ( data['status'] == 1 ) {                            
                                MyTasks.dom.error_msg.style.display = 'none';
                                MyTasks.dom.success_msg.style.display = 'block';
                                if (MyTasks.dom.link_under_button)
                                    MyTasks.dom.link_under_button.innerHTML = "Написать модераторам";
                                popup.dom.popup.style.display = 'none';
                                MyTasks.dom.button.style.display = 'none';
                            } else {
                                popup.alert(data['error'], 'complete-task');
                                if (typeof data['task'] !== 'undefined') {
                                    if (typeof data['task']['remained_time'] !== 'undefined') {                        
                                        MyTasks.dom.error_msg.style.display = 'block';
                                        MyTasks.dom.success_msg.style.display = 'none';
                                        MyTasks.activate(data);
                                        popup.dom.popup.style.display = 'none';
                                        MyTasks.dom.button.style.display = 'none';
                                    }
                                }                
                            }                        
                        } else {
                            console.log('При попытке сдать задачу нет ответа от сервера!');
                        }
                    } else {
                        console.log('При попытке сдать задачу произошла ошибка!');                
                    }
                    document.querySelector('.complete-task .button').style.display = 'block';
                    document.querySelector('.complete-task .loading').style.display = 'none';
                }
            }
        }
    }    