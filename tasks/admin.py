from django.contrib import admin
from .models import Task, TaskLink

class TaskLinkInline(admin.StackedInline):
        model = TaskLink                
        extra = 0
        
class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'cost', 'duration', 'completed', 'average_vote')
    inlines = [TaskLinkInline, ]

admin.site.register(Task, TaskAdmin)