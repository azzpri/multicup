import json
from .models import Task
from challenges.models import Challenge, ChallengeHistory
from challenges.views import check_active_challenge, challenge_to_material
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.forms.models import model_to_dict
from django.utils import timezone
from front.templatetags.multicup import declension, task_duration, slider_content
from members.models import Member, Juror
from results.models import Result, ResultVote

# Получить список задач
def task_list(request = None):
    
    if request:
        
        tasks = Task.objects.values('pk', 'title', 'short_description', 'cost', 'completed', 'average_vote', 'format')
        list_tasks = list(tasks)
        list_tasks = json.dumps(list_tasks, ensure_ascii=False)
        
        return HttpResponse(list_tasks, content_type="application/json; encoding=utf-8")

    return Task.objects.all()

def date_handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError

# Просмотр задачи и соревнования
def view(request, task_pk):
    response_data = {'task': 0}
    
    if request.user.is_authenticated:
        member = Member.objects.filter(user = request.user).first()
    else:
        member = False
    
    task = Task.objects.filter(pk = task_pk).first()    
    task_as_dict = model_to_dict(task)
    
    if member and member.is_juror():
        task_as_dict['links'] = [l.__str__() for l in task.links.all()]
    else:
        del task_as_dict['full_description']
        
    response_data['task'] = task_as_dict        

    return HttpResponse(
        json.dumps(response_data, ensure_ascii = False, default = date_handler),
            content_type='application/json; charset=utf-8')
    
    
# Просмотр задачи и соревнования
def user_task(request):
    
    response_data = {'task': 0, 'challenge': 0}    
    
    member_challenges = Challenge.objects.filter(member__user = request.user).order_by('-started_at')
    
    if member_challenges.first(): 
        if member_challenges.first().status not in ['accepted', 'scored']:
            challenge = member_challenges.first()
            unpublished = member_challenges.filter(status__in = ['wom', 'decline']).exclude(id = challenge.id)            
        else:
            unpublished = member_challenges.filter(status__in = ['wom', 'decline'])
            challenge = None
        
        published = member_challenges.filter(status__in = ['scored', 'accepted'])
        
        response_data['unpublished_materials'] = slider_content(
            [challenge_to_material(m) for m in unpublished],
            'materials')
        
        response_data['published_materials'] = slider_content(
            [challenge_to_material(m) for m in published],
            'materials')                               
                
        if challenge:
            task = challenge.task
            task_as_dict = model_to_dict(task)
            
            task_as_dict['links'] = [l.__str__() for l in task.links.all()]                                
            task_as_dict['subtitle'] =  task.format_verbose() + "&nbsp;&nbsp;" + task.cost_declension() \
                + "&nbsp;&nbsp;" + task_duration(task.duration_in_seconds())
            task_as_dict['completed'] = "Уже " + declension(task.completed, "сделал", "", "и", "и") \
                + " " + str(task.completed) + declension(task.completed, " человек", "", "а", "")
            
            if challenge.status in ['initial', 'extra']:
                remained_time = int((challenge.time_limit - timezone.now()).total_seconds())
                    
                if remained_time > 0:
                    task_as_dict['remained_time'] = "До сдачи задания осталось " + task_duration(remained_time)
                else:
                    if challenge.status == 'initial':
                        check_active_challenge(member_challenges, challenge)
                        response_data['member_amount'] = challenge.member.amount_declension()
                    if member_challenges.count() == 1:
                        task_as_dict['member_lost'] = 0
                    else:                                            
                        task_as_dict['member_lost'] = task.cost
    
                    task_as_dict['remained_time'] = 0
            elif challenge.status == 'decline':
                history = ChallengeHistory.objects.filter(challenge = challenge).last()
                if history:
                    task_as_dict['decline_description'] = history.description;
        
            response_data['challenge'] = model_to_dict(challenge)
            response_data['task'] = task_as_dict
        
                
    return HttpResponse(
        json.dumps(response_data, ensure_ascii = False, default = date_handler),
            content_type='application/json; charset=utf-8')


# Получить соревнования пользователя
def member_tasks(request, member_pk):
    response = {'status': 0} 
    
    if not request.is_ajax():
        response['error'] = 'Поддерживается только Ajax'
    else:
        member = Member.objects.filter(pk=member_pk)
        
        if len(member) != 1:
            response['error'] = 'При запросе Участника произошла ошибка'
        else:
            member = member.first()
            challenges = Challenge.objects.filter(member=member)
            
            tasks_ids = [c.task_id for c in challenges]
            tasks = Task.objects.filter(id__in=tasks_ids).values('pk', 'title', 'short_description', 'cost', 'completed', 'average_vote', 'format')
            response = {'status' : 1, 'tasks' : list(tasks)}
            
            if member.user_id == request.user.id:
                response['challenges'] = list(challenges.values('pk', 'task', 'status'))
    
    response = json.dumps(response, ensure_ascii=False)
    return HttpResponse(response, content_type="application/json; encoding=utf-8")

# Получить неоцененные соревнования и задачи жюри
def unvoted_tasks(request):
    response = {'status': 0}
    
    if not request.is_ajax():
        response['error'] = 'Поддерживается только Ajax'
    else:
        member = Member.objects.filter(pk=request.user.pk)
        
        if len(member) != 1:
            response['error'] = 'Данный пользователь не является Участником'
        else:
            member = member.first()
            juror = Juror.objects.filter(member=member)
            
            if len(juror) != 1:
                response['error'] = 'Данный пользователь не является Жюри'            
            else:
                juror = juror.first()
                
                resultvotes = ResultVote.objects.filter(juror=juror)
                results_ids = [r.result_id for r in resultvotes]
                results = Result.objects.filter(id__in=results_ids)
                
                tasks_ids = []
                challenges_ids = []
                for r in results:
                    tasks_ids.append(r.task_id)
                    challenges_ids.append(r.challenge_id)
                    
                tasks = Task.objects.filter(id__in=tasks_ids).values('pk', 'title', 'short_description', 'cost', 'completed', 'average_vote', 'format')
                challenges = Challenge.objects.filter(id__in=challenges_ids).values('pk', 'task', 'status')
                
                response = {'status' : 1, 'tasks' : list(tasks), 'challenges' : list(challenges)}
                                
    response = json.dumps(response, ensure_ascii=False)
    return HttpResponse(response, content_type="application/json; encoding=utf-8")