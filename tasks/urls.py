from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^list/$', views.task_list, name = 'list'),
    url(r'^view/(?P<task_pk>[0-9]+)$', views.view),
    url(r'^user_task/$', views.user_task),
    url(r'^member/(?P<member_pk>[0-9]+)$', views.member_tasks, name = 'member'),
    url(r'^juror/$', views.unvoted_tasks, name = 'juror'),
]