from django.db import models
from front.templatetags.multicup import declension

# формат задачи
FORMAT_CHOICES = (    
    ('people', 'Люди',),
    ('look', 'Наблюдение',),
    ('numbers', 'Цифры',),
    ('docs', 'Документы',),
    ('repack', 'Переупаковка',),
    ('tech', 'Технологии',),
)
    
class Task(models.Model):
    class Meta:
        verbose_name = 'Задание'
        verbose_name_plural = 'Задания'

    title = models.CharField(max_length=128, verbose_name="Название") 
    short_description = models.TextField(verbose_name="Короткое описание")
    full_description = models.TextField(verbose_name="Полное описание")   
    cost = models.IntegerField(default=0, verbose_name="Стоимость (баллов)")         
    completed = models.IntegerField(default=0, verbose_name="Количество выполнивших")
    average_vote = models.FloatField(default=0, verbose_name="Средняя оценка")
    duration = models.IntegerField(default=0, verbose_name="Часов на выполнение")    
    format = models.CharField(
        max_length=30,
        choices = FORMAT_CHOICES,
        verbose_name="Формат задачи",
        default = 'people'
    )
    
    def __str__(self):
        return self.title
    
    def cost_declension(self):
        return str(self.cost) + " " + declension(self.cost, "балл", "", "а", "ов")
    
    def format_verbose(self):
        formats = dict((k, v) for k, v in FORMAT_CHOICES)
        return formats[self.format]
    
    def duration_in_seconds(self):
        return self.duration*3600

class TaskLink(models.Model):
    class Meta:
        verbose_name = 'Ссылка в задании'
        verbose_name_plural = 'Ссылки в заданиях'
            
    task = models.ForeignKey(Task, on_delete=models.CASCADE, verbose_name="Задача", related_name='links')
    url = models.CharField(max_length=200, verbose_name="Ссылка на материалы")
    name = models.CharField(max_length=100, verbose_name="Отображаемое название", blank=True)
    
    def __str__(self):
        if self.name :
            name = self.name
        else :
            name = self.url
        
        return '<a href="%s">%s</a>' % (
            self.url,
            name,
        )