from .models import Task
from . import views
import datetime
from challenges.models import Challenge
from django.contrib.auth.models import User
from django.test import TestCase, override_settings
from django.test.client import Client
from django.utils import timezone
import json
from members.models import Member, Juror
from multicup.settings import SERVER_URL
from results.models import Result, ResultVote

def responseContentToData(content):
    answer = content.decode('utf-8')
    return json.loads(answer)    

class TaskTestCase(TestCase):
    def setUp(self):           
        task1 = Task.objects.create(
            title="Тестовая задача 1",
            short_description="Короткое описание тестовой задачи",
            full_description="Полное описание тестовой задачи",
            cost=300,
            completed=20,
            average_vote=10,
            duration=20,
            materials="http://testserver.cpm/docs"
        )
        task2 = Task.objects.create(
            title="Тестовая задача 2",
            short_description="Короткое описание тестовой задачи 2",
            full_description="Полное описание тестовой задачи 2",
            cost=500,
            completed=10,
            average_vote=12,
            duration=15,
            materials="http://testserver.cpm/docs2"
        )
        
        user = User.objects.create(username='testusername123')
        member = Member.objects.create(user = user)
        
        challenge_end_time = timezone.now() + datetime.timedelta(hours=task2.duration)
        challenge = Challenge.objects.create(task=task2, member=member, time_limit=challenge_end_time)
        
        user2 = User.objects.create(username='testusername2')
        member2 = Member.objects.create(user = user2)
        juror = Juror.objects.create(member=member2)
        
        result = Result.objects.create(task=task2, challenge=challenge, member=member, description='Описание')
        resultvote = ResultVote.objects.create(result=result, juror=juror)
        
    # Проверяем, что задачи добавляются в базу и их можно получить
    def test_get_taks_list(self):
        # тестируем прямой вызов
        tasks = views.task_list()
        self.assertEqual(len(tasks), 2)
        
        # тестируем Ajax
        client = Client()
        response = client.post(SERVER_URL + 'tasks/list/', {}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        task_data = responseContentToData(response.content)
        self.assertEqual(task_data[0]['completed'], 20)
    
    # Проверяем получение задачи
    def test_get_task(self):                
        user = User.objects.get(username='testusername123')
                
        # проверяем описание задачи без challenge
        task1 = Task.objects.get(title='Тестовая задача 1')
        client = Client()
        client.force_login(user)
        response = client.post(SERVER_URL + 'tasks/view/' + str(task1.pk), {}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        task_data = responseContentToData(response.content)
        self.assertEqual(task_data['challenge'], 0)
        self.assertEqual(task_data['task']['cost'], 300)
        
        check_fd = False
        
        if 'full_description' in task_data['task']:
            check_fd = True
            
        self.assertEqual(check_fd, False)
        
        # проверяем описание задачи c challange
        task2 = Task.objects.get(title='Тестовая задача 2')
        response = client.post(SERVER_URL + 'tasks/view/' + str(task2.pk), {}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        task_data = responseContentToData(response.content)
        self.assertEqual(task_data['challenge']['task'], task2.pk)
        self.assertEqual(task_data['task']['full_description'], 'Полное описание тестовой задачи 2')
        
    # Проверяем получение задач Участника
    def test_get_member_tasks(self):
        user = User.objects.get(username='testusername123')
        member = Member.objects.get(user=user)
        
        client = Client()
        response = client.post(SERVER_URL + 'tasks/member/{0}'.format(member.id), {}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        task_data = responseContentToData(response.content)
        self.assertEqual(task_data['tasks'][0]['title'], 'Тестовая задача 2')
        
    # Проверяем неоцененные задачи Жюри
    def test_get_juror_tasks(self):
        user2 = User.objects.get(username='testusername2')
                
        client = Client()
        client.force_login(user2)
        response = client.post(SERVER_URL + 'tasks/juror/', {}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        task_data = responseContentToData(response.content)
        self.assertEqual(task_data['tasks'][0]['title'], 'Тестовая задача 2')