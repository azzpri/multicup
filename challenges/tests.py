import datetime, json, sys
from challenges import views
from challenges.models import Challenge
from django.utils import timezone
from django.test import TestCase, override_settings
from django.test.client import Client
from django.contrib.auth.models import User, Group
from django.core import mail
from members.models import Member, Juror
from multicup.settings import SERVER_URL, EMAIL_HOST_USER
from results.models import Result, ResultVote
from tasks.models import Task

def responseContentToData(content):
    answer = content.decode('utf-8')
    return json.loads(answer)    

class ChallengeTestCase(TestCase):
    def setUp(self):                
        task = Task.objects.create(
                title = "Тестовая задача 1",
                short_description = "Короткое описание тестовой задачи",
                full_description = "Полное описание тестовой задачи",
                cost = 300,
                completed = 0,
                average_vote = 0,
                duration = 20
            )
        
        user1 = User.objects.create(username = 'testmember1')
        member1 = Member.objects.create(user = user1, amount = 300)
        
        user2 = User.objects.create(username = 'testjuror1')
        member2 = Member.objects.create(user = user2, amount = 0)
        juror1 = Juror.objects.create(member = member2, formats = '')
        
        user3 = User.objects.create(username = 'testjuror2')
        member3 = Member.objects.create(user = user3, amount = 0)
        juror2 = Juror.objects.create(member = member3, formats = '')

    # Работа функции checkAmount(challenge_id) как считается рейтинг пользователя
    def test_check_amount(self):        
        member1 = Member.objects.get(user__username = 'testmember1')
        task1 = Task.objects.get(title = 'Тестовая задача 1')
        juror1 = Juror.objects.get(member__user__username = 'testjuror1')
        juror2 = Juror.objects.get(member__user__username = 'testjuror2')
        
        challenge1 = Challenge.objects.create(
            status = 'wm',
            member = member1,
            task = task1,
            time_limit = '2017-12-30 12:30:28'
        )
        challenge1.save()
        
        result1 = Result.objects.create(
            task = task1,
            challenge = challenge1,
            member = member1,
            description = 'Описание результата',
            image = 'test.png',
            title = 'Название результата',
            url = 'Ссылка',
            url_title = 'Название cсылки'            
        )
        result1.save()
        
        member_amount1 = member1.amount
        challenge_total_amount1 = challenge1.total_amount
        challenge_vote_amount1 = challenge1.vote_amount
        
        resultvote1 = ResultVote.objects.create(
            result = result1, 
            juror = juror1,
            voted = False,
            vote1 = 0,
            vote2 = 0,
            vote3 = 0,
            vote4 = 0,
            vote5 = 0,
            average_vote = 0,
        )
        resultvote1.save()
        
        resultvote2 = ResultVote.objects.create(
            result = result1, 
            juror = juror2,
            voted = True,
            vote1 = 4,
            vote2 = 4,
            vote3 = 4,
            vote4 = 4,
            vote5 = 4,
            average_vote = 5.0,
        )
        resultvote2.save()
        
        views.checkAmounts(challenge1.id)
        
        member1 = Member.objects.get(user__username = 'testmember1')
        challenge1 = Challenge.objects.get(id = challenge1.id)
        
        self.assertEqual(member1.amount, 450)
        self.assertEqual(challenge1.total_amount, 150)
        self.assertEqual(challenge1.vote_amount, 150)
        
        resultvote1.voted = True
        resultvote1.average_vote = 3.0
        resultvote1.save()
        
        views.checkAmounts(challenge1.id)
        
        member1 = Member.objects.get(user__username = 'testmember1')
        challenge1 = Challenge.objects.get(id = challenge1.id)
        
        self.assertEqual(member1.amount, 420)
        self.assertEqual(challenge1.total_amount, 120)
        self.assertEqual(challenge1.vote_amount, 120)

    # # Проверяем добавление Соревнования
    # def test_add_challenge(self):        
    #     task = Task.objects.get(title="Тестовая задача 1")
    #     user = User.objects.get(username='testusername123')
    #                     
    #     client = Client()
    #     client.force_login(user)        
    #     response = client.post(SERVER_URL + 'challenges/add/' + str(task.id), {}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
    #     data = responseContentToData(response.content)
    #     self.assertEqual(data['status'], 1)
    #     
    # # Проверяем просмотр Соревнования
    # def test_view_challenge(self):
    #     task = Task.objects.get(title="Тестовая задача 1")
    #     user = User.objects.get(username='testusername123')
    #     member = Member.objects.get(user = user)
    #     client = Client()
    #     client.force_login(user)
    #     response1 = client.post(SERVER_URL + 'challenges/add/' + str(task.id), {}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
    #     data1 = responseContentToData(response1.content)
    #     
    #     response2 = client.post(SERVER_URL + 'challenges/view/' + str(data1['challenge_pk']), {}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
    #     data2 = responseContentToData(response2.content)
    #     self.assertEqual(data2['challenge']['id'], data1['challenge_pk'])
    #     
    # # Проверяем завершение Соревнования
    # def test_complete_challenge(self):        
    #     task = Task.objects.get(title="Тестовая задача 1")
    #     user = User.objects.get(username='testusername123')
    #     member = Member.objects.get(user = user)
    #     client = Client()
    #     client.force_login(user)
    #     response1 = client.post(SERVER_URL + 'challenges/add/' + str(task.id), {}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
    #     data1 = responseContentToData(response1.content)
    #     
    #     response2 = client.post(
    #         SERVER_URL + 'challenges/complete/' + str(data1['challenge_pk']),
    #         {'description':'Описание Результата',},
    #         **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
    #     data2 = responseContentToData(response2.content)        
    #     self.assertEqual(data2['result_id'], 1)
    #             
    #     sys.stderr.write(repr(mail.outbox[0].subject) + '\n')
    # 
    # # Проверяем завершение Соревнования
    # def test_challenge_contest(self):
    #     
    #     # создаем Challenge
    #     task = Task.objects.get(title="Тестовая задача 1")
    #     user = User.objects.get(username='testusername123')
    #                     
    #     client = Client()
    #     client.force_login(user)        
    #     response = client.post(SERVER_URL + 'challenges/add/' + str(task.id), {}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
    #     data = responseContentToData(response.content)
    #     
    #     # Отклоняем Challenge
    #     challenge = Challenge.objects.get(pk=data['challenge_pk'])
    #     challenge.status = 'decline'
    #     challenge.save()
    #     
    #     # Открываем спор
    #     response2 = client.post(SERVER_URL + 'challenges/contest/' + str(data['challenge_pk']), {}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
    #     data2 = responseContentToData(response2.content)                
    #     self.assertEqual(data2['status'], 1)
    #     
    #     sys.stderr.write(repr(mail.outbox[0].subject) + '\n')