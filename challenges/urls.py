from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^add/(?P<task_pk>[0-9]+)$', views.add, name='add'),
    url(r'^view/(?P<challenge_pk>[0-9]+)$', views.view, name='view'),
    url(r'^complete/(?P<challenge_pk>[0-9]+)$', views.complete, name='complete'),
    url(r'^contest/(?P<challenge_pk>[0-9]+)$', views.contest, name='contest'),
    url(r'^add_time/(?P<challenge_id>[0-9]+)$', views.add_time),
    url(r'^accept/(?P<challenge_id>[0-9]+)$', views.accept),
]