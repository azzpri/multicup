import datetime, front
from .models import Challenge, ChallengeHistory
from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils import timezone
from results.models import Result
from members.views import chooseJuror, check_member_rating
from members.models import MemberAmountHistory
from multicup import settings

class ResultInline(admin.StackedInline):
        model = Result
        exclude = ('task','member', 'url', 'url_title')
        readonly_fields = ('title','description', 'clickable_url', 'image','likes' )
        extra = 0
        
        def clickable_url(self, obj):
            return '<a href="%s">%s</a>' % (obj.url, obj.url_title)
        
        clickable_url.allow_tags = True
        clickable_url.short_description = "Ссылка на материалы"


class ChallengeForm(forms.ModelForm):
    moderator_comment = forms.CharField(max_length=400, widget=forms.Textarea, label='Комментарий', required=False)
    time = forms.CharField(max_length=2, label='Добавить участнику время(ч)', required=False)        
    class Meta:
        model = Challenge
        exclude = ['name']
            
    def clean(form):
        
        status = form.cleaned_data['status']
        
        if form.previous_status and status == form.previous_status:
            raise forms.ValidationError("Статус остался прежним")
        
        if status == 'decline' and len(form.cleaned_data['moderator_comment']) == 0:
            raise forms.ValidationError("Поле 'Комментарий' обязательно для заполнения при отклонении задачи")
        elif status == 'extra':
            if len(form.cleaned_data['time']) == 0:
                raise forms.ValidationError("Поле 'Добавить участнику время' обязательно для заполнения при отклонении задачи")
            elif form.previous_status in ['decline', 'wom']:
                if front.views.CUP_DATES['challenge_complete_finish'] and \
                    front.views.CUP_DATES['challenge_complete_finish'] < timezone.now():
                        raise forms.ValidationError("Конкурс заканчивается. Вы не можете больше добавлять время для задач")
                                
                challenge = Challenge.objects.filter(id = form.id).first()
                wom_history = ChallengeHistory.objects.filter(challenge_id__in = [x.id for x in Challenge.objects.filter(member = challenge.member)], status = "wom")
                if wom_history.count() > 1 or form.previous_status == 'decline':
                    return_amount_to_member = True                
        elif status == 'accepted':
            if front.views.CUP_DATES['rating_change_finish'] and \
                    front.views.CUP_DATES['rating_change_finish'] < timezone.now():
                        raise forms.ValidationError("Конкурс закончился. Задачи больше нельзя принимать")
            choose_juror_result = chooseJuror(form.id)
            if choose_juror_result['status'] == 0:
                raise forms.ValidationError(choose_juror_result['error'])
            if form.previous_status == 'decline':
                return_amount_to_member = True                
            send_message = True
            
        if 'send_message' in locals() or 'return_amount_to_member' in locals():
            challenge = Challenge.objects.filter(id = form.id).first()
            
            if 'return_amount_to_member' in locals():
                challenge.member.amount += challenge.task.cost
                challenge.member.message = 'Вам добавлено время на выполнение задания и возвращено {0} баллов'.format(challenge.task.cost)
                if 'send_message' in locals():
                    challenge.member.message = 'Ваши материалы опубликованы и вам возвращено {0} баллов'.format(challenge.task.cost)
                    challenge.member.challenge_completed += 1
                check_member_rating(challenge.member)
                
                MemberAmountHistory.objects.create(
                    member = challenge.member,
                    action = 'add',
                    description = challenge.member.message
                )
            else:
                challenge.member.message = 'Поздравляем! Ваш материал был опубликован.'
                challenge.member.challenge_completed += 1
                
            challenge.member.save()
            
        return form.cleaned_data

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            self.id = kwargs['instance'].id
            self.previous_status = kwargs['instance'].status
        super(ChallengeForm, self).__init__(*args, **kwargs)
        
class ChallengeAdmin(admin.ModelAdmin):
    list_display = ('challenge_pk', 'task_title', 'resulttitle', 'username', 'status', 'started_at')
    readonly_fields = ('task_title', 'username', 'vote_amount', 'likes_amount', 'total_amount', 'time_limit')
    search_fields = ['member__user__first_name', 'member__user__last_name', 'member__user__username', 'task__title', 'status']
    list_filter = ('task__title', 'status',)
    exclude = ('task','member')
    inlines = [ResultInline, ]
    form = ChallengeForm
    
    def challenge_pk(self, obj):
        return obj.pk
    
    challenge_pk.admin_order_field  = 'challenge_pk' 
    challenge_pk.short_description = '№'
    
    def task_title(self, obj):
        return obj.task.title
    
    task_title.short_description = 'Задача'        
    
    def username(self, obj):
        return obj.member.user.first_name + " " + obj.member.user.last_name + " (" + obj.member.user.username + ")"
    
    username.short_description = 'Пользователь'
    
    def resulttitle(self, obj):
        result = obj.result_set.all().last()
        if result:
            return result.title
        else:
            return ""

    # переопределяем метод сохранения
    def save_model(self, request, obj, form, change):
    
        description = ''
        email_context = {
            'server_url' : settings.SERVER_URL
        }
                
        # Если модератор перевел Соревнование в статус Принято
        if obj.status == 'accepted':
            description = 'Модератор принял Соревнование'
            obj.task.completed += 1
            obj.task.save()
                        
            # готовим данные для письма Пользователю
            if settings.MULTICUP['NOTIFICATIONS']['CHALLENGE_ACCEPT']:
                email_context['challenge_id'] = obj.id
                email_context['title'] = 'Ваш материал опубликован на сайте конкурса'
                email_context['template'] = 'email/challenge-accept.html'
                               
        # Если модератор перевел Соревнование в статус Отклонено
        if obj.status == 'decline':
            description = 'Комментарий модератора: ' + form.cleaned_data['moderator_comment']
            member = obj.member
            member.amount -= obj.task.cost
            member.message = 'Ваша задача была отклонена модератором и вы потеряли {0} баллов'.format(obj.task.cost)
            member.save()
            
            MemberAmountHistory.objects.create(
                member = member,
                action = 'remove',
                description = member.message
            )
            
            check_member_rating(obj.member)
            
            # готовим данные для письма Пользователю
            if settings.MULTICUP['NOTIFICATIONS']['CHALLENGE_DECLINE']:
                email_context['challenge_id'] = obj.id
                email_context['comment'] = form.cleaned_data['moderator_comment']
                email_context['title'] = 'Ваш конкурсный материал отклонен модератором'
                email_context['template'] = 'email/challenge-decline.html'
        
        # Если модератор перевел Соревнование в статус Добавлено время
        if obj.status == 'extra':                        
            # добавляем время для ответа
            description = 'Модератор добавил время участнику (ч): +' + str(form.cleaned_data['time'])
            
            obj.time_limit = timezone.now() + datetime.timedelta(hours = int(form.cleaned_data['time']))
            
            # готовим данные для письма Пользователю
            if settings.MULTICUP['NOTIFICATIONS']['CHALLENGE_EXTRA']:
                email_context['title'] = 'Время на выполнение задачи продлено'
                email_context['template'] = 'email/challenge-extend.html'
                
        # Делаем запись в истории Соревнования
        challenge_history = ChallengeHistory(challenge = obj, status = obj.status, description = description)
        challenge_history.save()
                                
        obj.save()
        
        # отправка письма Пользователю
        if 'template' in email_context and obj.member.subscription:
                message = render_to_string(email_context['template'], email_context)
                                
                send_mail(
                        email_context['title'],
                        message,
                        settings.EMAIL_HOST_USER,
                        [obj.member.user.email, settings.EMAIL_HOST_USER],
                        html_message = message,                        
                )
                
    
class ChallengeHistoryAdmin(admin.ModelAdmin):
    list_display = ('challenge_pk', 'task_title', 'username', 'status', 'description_short', 'changed_at')
    list_filter = ('challenge__member__user__last_name',)
    search_fields = ['challenge__member__user__first_name', 'challenge__member__user__last_name', 'challenge__member__user__username', 'challenge__task__title', 'challenge__id']
    readonly_fields = ('challenge_pk', 'status', 'description')
    exclude = ('challenge',)

    def challenge_pk(self, obj):
        return obj.challenge.id
    
    challenge_pk.admin_order_field  = 'challenge_id' 
    challenge_pk.short_description = 'Соревнование'
    
    def task_title(self, obj):
        return obj.challenge.task.title
    
    task_title.short_description = 'Задача'
    
    def username(self, obj):
        return obj.challenge.member.user.first_name + " " + obj.challenge.member.user.last_name + " (" + obj.challenge.member.user.username + ")"
    
    username.short_description = 'Пользователь'
    
    def description_short(self, obj):
        return obj.description[:80]
    
    description_short.short_description = 'Описание'
    
admin.site.register(Challenge, ChallengeAdmin)
admin.site.register(ChallengeHistory, ChallengeHistoryAdmin)

