import datetime, json, front
from challenges.models import Challenge, ChallengeHistory
from django.contrib.auth.models import Group
from django.core import serializers
from django.core.mail import send_mail
from django.forms.models import model_to_dict
from django.http import JsonResponse, HttpResponse
from django.template.loader import render_to_string
from django.utils import timezone
from front.templatetags.multicup import declension, task_duration, has_group
from members.models import Member, MemberAmountHistory, Juror
from members.views import chooseJuror, check_member_rating
from multicup import settings
from results.models import Result, ResultVote, can_vote_for_material
from tasks.models import Task

#   Сконвертировать Challenge в Материал
#       challenge - объект Challenge
#       return {"challenge": {}, "task": {}, "result"}        
def challenge_to_material(challenge):    
    material = {}
    material['challenge'] = challenge
    material['task'] = challenge.task
    material['result'] = challenge.result_set.all().last()
    material['user'] = challenge.member.user
    material['mine'] = 0
    return material

# Взять задачу и создать Соревнование
def add(request, task_pk):
    
    response = {'status': 0, 'challenge_pk': 0}
        
    if front.views.CUP_DATES['challenge_add_start'] > timezone.now():
        delta = front.views.CUP_DATES['challenge_add_start'] - timezone.now()
        response['error'] = 'Конкурс начнется через {0} и вы сможете взять первую задачу'.format(task_duration(delta.seconds))
    elif front.views.CUP_DATES['challenge_add_finish'] < timezone.now():
        response['error'] = 'Новые задачи брать нельзя. Конкурс подходит к концу или завершен.'
    else:
        user_pk = request.user.pk    
        member = Member.objects.filter(user__pk = user_pk).first()
        task = Task.objects.filter(pk = task_pk).first()
        
        if not member or not task:
            response['error'] = 'Такой задачи нет или пользователь не является участником'
        else:        
            member_challenges = Challenge.objects.filter(member = member)
            member_active_challenges = member_challenges.filter(status__in = ['initial', 'extra'])
                    
            if not member.city:
                response['error'] = 'Чтобы принять участие в соревновании необходимо заполнить профиль'
            else:
                if member.amount < task.cost:
                    response['error'] = 'Недостаточно баллов, чтобы взять задание'
                else:
                    if member_challenges.filter(task = task).count() > 0:
                        response['error'] = 'Повторно выполнить задачу невозможно'
                    else:            
                        if member_active_challenges.count() != 0:
                            if member_active_challenges.count() > 1:
                                response['error'] = 'Упс! У вас 2 активных задачи одновременно. Напишите модераторам, чтобы исправить ошибку'
                            else:
                                if check_active_challenge(member_challenges, member_active_challenges.first()):
                                    response['error'] = "У вас есть активная задача. Выполните ее, прежде, чем брать следующую"
                                else:
                                    member_amount = Member.objects.filter(user__pk = user_pk).first().amount_declension()
                                
                        if 'error' not in response:
                            # Создаем новое Соревнование
                            challenge_end_time = timezone.now() + datetime.timedelta(hours=task.duration)
                            challenge = Challenge(task=task, member=member, status='initial', time_limit=challenge_end_time)
                            challenge.save()
                            
                            # Делаем запись в истории Соревнования
                            challenge_history = ChallengeHistory(challenge=challenge, status='initial', description='Участник взял задачу')
                            challenge_history.save()
                            
                            task_as_dict = model_to_dict(task)
                    
                            task_as_dict['subtitle'] =  task.format_verbose() + "  " + task.cost_declension() \
                                    + "  " + task_duration(task.duration_in_seconds())
                            task_as_dict['completed'] = "Уже " + declension(task.completed, "сделал", "", "и", "и") + " " + str(task.completed) + declension(task.completed, " человек", "", "а", "")
                            
                            time_delta = challenge.time_limit - timezone.now()
                            
                            task_as_dict['remained_time'] = "До сдачи задания осталось " + task_duration(time_delta.seconds)
                            
                            member_materials = front.views.member_unpublished_materials(request)
                                    
                            response = {
                                'status'                : 1,
                                'challenge'             : model_to_dict(challenge, None, ('time_limit',)),
                                'task'                  : task_as_dict,                        
                                'unpublished_materials' : member_materials['unpublished_materials'],
                                'published_materials'   : member_materials['published_materials']
                            }
                            
                            if 'member_amount' in locals():
                                response['member_amount'] = member_amount
                    
    response = json.dumps(response, ensure_ascii=False)
    return HttpResponse(response, content_type="application/json; encoding=utf-8")

#   Добавить время на задание
#       /challenges/add_time/<challenge_id>, challenge_id - идентификатор Challenge
#       POST:
#           time - время, которое нужно добавить
def add_time (request, challenge_id):
    response = {'status' : 0}
    if not request.user.is_authenticated() or not has_group(request.user, 'Moderator'):
        response['error'] = 'Пользователь не авторизован или не является модератором'
    else:
        time = request.POST.get('time')
        if not time:
            response['error'] = 'Параметр время не задан'
        else:
            if front.views.CUP_DATES['challenge_complete_finish'] and \
            front.views.CUP_DATES['challenge_complete_finish'] < timezone.now():
                response['error'] = 'Продлевать задания невозможно: конкурс уже закончился.'
            else:
                challenge = Challenge.objects.filter(id = challenge_id).first()
                if challenge and challenge.status in ['wom', 'decline']:
                
                    challenge.member.message = 'Вам добавлено время на выполнение задания'
                    
                    wom_history = ChallengeHistory.objects.filter(challenge_id__in = [x.id for x in Challenge.objects.filter(member = challenge.member)], status = "wom")
                    if wom_history.count() > 1 or challenge.status == 'decline':
                        challenge.member.amount += challenge.task.cost
                        challenge.member.message += ' и возвращено {0} баллов'.format(challenge.task.cost)
                        MemberAmountHistory.objects.create(
                            member = challenge.member,
                            action = 'add',
                            description = challenge.member.message
                        )
                    
                    challenge.member.save()
                    challenge.time_limit = timezone.now() + datetime.timedelta(hours = int(time))                
                    challenge.status = 'extra'
                    challenge.save()
                    
                    description = 'Модератор добавил время участнику (ч): +' + str(time)
                    challenge_history = ChallengeHistory(challenge = challenge, status = 'extra', description = description)
                    challenge_history.save()
                    
                    # готовим данные для письма Пользователю
                    if settings.MULTICUP['NOTIFICATIONS']['CHALLENGE_EXTRA']:
                        email_context = {
                            'server_url'    : settings.SERVER_URL,
                            'title'         : 'Время на выполнение задачи продлено',
                            'template'      : 'email/challenge-extend.html'
                        }
                        message = render_to_string(email_context['template'], email_context)                     
                        send_mail(
                                email_context['title'],
                                message,
                                settings.EMAIL_HOST_USER,
                                [challenge.member.user.email, settings.EMAIL_HOST_USER],
                                html_message = message,                        
                        )
                    response['status'] = 1
                else:
                    response['error'] = 'Соревнования не существует или у него неверный статус'
    response = json.dumps(response, ensure_ascii = False)
    return HttpResponse(response, content_type = "application/json; encoding=utf-8")

#   Принять или отклонить Соревнование
#       /challenges/accept/<challenge_id>, challenge_id - идентификатор Challenge
#       POST:
#           action - Действие ('accepted' или 'decline')
#           comment - Комментарий (обязатен для action = 'decline')
def accept (request, challenge_id):
    response = {'status' : 0}
    if not request.user.is_authenticated() or not has_group(request.user, 'Moderator'):
        response['error'] = 'Пользователь не авторизован или не является модератором'
    else:
        action = request.POST.get('action')
        comment = request.POST.get('comment')
        if action == 'accepted' or (action == 'decline' and comment):            
            if front.views.CUP_DATES['challenge_complete_finish'] and \
            front.views.CUP_DATES['challenge_complete_finish'] < timezone.now():
                response['error'] = 'Принимать/отклонять задание невозможно: конкурс уже закончился.'
            else:
                challenge = Challenge.objects.filter(id = challenge_id).first()
                if challenge and challenge.status == 'wm':
                    email_context = {
                        'server_url' : settings.SERVER_URL
                    }
                    if action == 'accepted':
                        choose_juror_result = chooseJuror(challenge.id)
                        if choose_juror_result['status'] == 0:
                            response['error'] = choose_juror_result['error']
                        else:
                            challenge.member.message = 'Поздравляем! Ваш материал был опубликован.'
                            challenge.member.challenge_completed += 1
                            challenge.member.save()
                            description = 'Модератор принял Соревнование'
                            challenge.task.completed += 1
                            challenge.task.save()
                            if settings.MULTICUP['NOTIFICATIONS']['CHALLENGE_ACCEPT']:                                
                                email_context['title'] = 'Ваш материал опубликован на сайте конкурса'
                                email_context['template'] = 'email/challenge-accept.html'
                    else:
                        description = 'Комментарий модератора: ' + comment
                        member = challenge.member                                                                        
                        member.amount -= challenge.task.cost
                        member.message = 'Ваша задача была отклонена модератором и вы потеряли {0} баллов'.format(challenge.task.cost)
                        member.save()
                        
                        MemberAmountHistory.objects.create(
                            member = member,
                            action = 'remove',
                            description = member.message
                        )
                        
                        check_member_rating(challenge.member)
                        if settings.MULTICUP['NOTIFICATIONS']['CHALLENGE_DECLINE']:                            
                            email_context['comment'] = comment
                            email_context['title'] = 'Ваш конкурсный материал отклонен модератором'
                            email_context['template'] = 'email/challenge-decline.html'                    
                    
                    if not 'error' in response:                        
                        email_context['challenge_id'] = challenge.id
                        challenge.status = action
                        challenge.save()
                        challenge_history = ChallengeHistory(challenge = challenge, status = action, description = description)
                        challenge_history.save()
                    
                        # готовим данные для письма Пользователю
                        if 'title' in email_context:                            
                            message = render_to_string(email_context['template'], email_context)                     
                            send_mail(
                                    email_context['title'],
                                    message,
                                    settings.EMAIL_HOST_USER,
                                    [challenge.member.user.email, settings.EMAIL_HOST_USER],
                                    html_message = message,
                            )
                        response['status'] = 1
                else:
                    response['error'] = 'Нет Соревнования или его нельзя принимать/отклонять'
        else:
            response['error'] = 'Параметр action или comment не задан'
                
    response = json.dumps(response, ensure_ascii = False)
    return HttpResponse(response, content_type = "application/json; encoding=utf-8")

# Проверка текущего статуса соревнования
# return True - задача еще действующая
# return False - задача просрочена
def check_active_challenge(member_challenges, active_challenge, **kwargs):

    timeout = False
    if front.views.CUP_DATES['challenge_complete_finish'] and \
        front.views.CUP_DATES['challenge_complete_finish'] < timezone.now():
            timeout = True
            
    if active_challenge.time_limit <= timezone.now() or timeout:
        active_challenge.status = 'wom'
        active_challenge.save()
        
        if not member_challenges:
            member_challenges = Challenge.objects.filter(member = active_challenge.member)
        
        wom_history = ChallengeHistory.objects.filter(challenge_id__in = [x.id for x in member_challenges], status = "wom")
        
        if wom_history.count() > 0:
            message = 'Вы не успели выполнить задачу в срок и потеряли '+ str(active_challenge.task.cost) +' баллов'
            active_challenge.member.amount -= active_challenge.task.cost
            
            member_history = MemberAmountHistory(member = active_challenge.member, action = "remove", description = message)
            member_history.save()
        else:
            message = 'Вы не успели выполнить задачу в срок'
            
        active_challenge.member.message = message
        active_challenge.member.save()
        
        if member_challenges.count() > 1:
            check_member_rating(active_challenge.member)
        
        # Добавляем событие
        challenge_history = ChallengeHistory(challenge = active_challenge, status = 'wom', description = 'Время выполнения задачи вышло')
        challenge_history.save()
        
        return False
    elif (active_challenge.time_limit - timezone.now()).seconds < 3600*12 and \
        'notify_on_deadline' in kwargs and not active_challenge.notified:
            active_challenge.notified = True
            active_challenge.save()
            email_context = {
                'server_url'    : settings.SERVER_URL,                
                'title'         : 'Не пропустите дедлайн по заданию',
                'email'         : active_challenge.member.user.email,
            }
            message = render_to_string('email/challenge-deadline.html', email_context)
            send_mail(
                email_context['title'],
                message,
                settings.EMAIL_HOST_USER,
                [active_challenge.member.user.email, settings.EMAIL_HOST_USER],
                html_message = message,
            )
        
    return True

# Получить статус Соревнования
def view(request, challenge_pk):
    
    response = {}
    juror = is_moderator = False
    
    challenge = Challenge.objects.filter(id = challenge_pk).first()
    
    if not challenge:
        response['error'] = "Не существует данного соревнования"
    else:
        
        if request.user.is_authenticated():
            juror = Juror.objects.filter(member__user = request.user).first()
            is_moderator = has_group(request.user, 'Moderator')
        
        if challenge.status in ['accepted', 'voted'] or \
            (request.user and challenge.member.user == request.user) or juror or is_moderator:
        
            response['challenge'] = model_to_dict(challenge, ('total_amount', 'status'))
            
            if juror or is_moderator:
                fields = ('full_description', 'title', 'links')
            else:
                fields = ('short_description', 'title')
            response['task'] = model_to_dict(challenge.task, fields)
            response['task']['format'] = challenge.task.format_verbose()
            response['task']['duration'] = task_duration(challenge.task.duration_in_seconds())
            response['task']['cost'] = challenge.task.cost_declension()
            if juror or is_moderator:
                response['task']['links'] = [l.__str__() for l in challenge.task.links.all()]              
            
            response['member'] = model_to_dict(challenge.member, ('id', 'rating',))
            response['member']['photo'] = challenge.member.photo.name
            response['member']['bio'] = challenge.member.user.first_name + " " + challenge.member.user.last_name
            
            # Если пользователь автор Соревнования, то показываем комментарий модератора
            if challenge.status == 'decline' and (challenge.member.user == request.user or is_moderator):
                history_record = ChallengeHistory.objects.filter(challenge = challenge, status = 'decline').only('description').first()
                if history_record:
                    response['challenge']['moderator_comment'] = history_record.description
                else:
                    response['challenge']['moderator_comment'] = ''
            
            result = Result.objects.filter(challenge = challenge)
            if result.count() > 0:
                
                response['result'] = result.values('description', 'url', 'likes', 'title', 'image', 'url_title').last()
                
                if juror and can_vote_for_material(juror, result.last()):
                    response['to_vote'] = 1
                else:
                    result_ids = [x for x in result.values_list('id', flat = 'True')]
                    response['votes'] = [vote for vote in ResultVote.objects.filter(result__id__in = result_ids, voted = True).values(
                        'average_vote', 'comment', 'juror__member__user__first_name', 'juror__member__user__last_name',
                        'juror__member__photo', 'juror__member__job', 'juror__member__company')]
                
            if is_moderator:
                response['moderate'] = 1            
                
        else:
            response['error'] = "У вас нет доступа к данному соревнованию"
    
    
    dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else None
    response = json.dumps(response, ensure_ascii=False, default=dthandler)
    return HttpResponse(response, content_type="application/json; encoding=utf-8")

# Сдать Результат для Соревнования
def complete(request, challenge_pk):    
        
    response = {'status': 0, 'result_pk': 0}
            
    user_pk = request.user.pk
        
    # Проверяем, что Пользователь является участником и у него есть такое задание    
    member_challenges = Challenge.objects.filter(member__user = request.user)
    challenge = member_challenges.filter(pk = challenge_pk, status__in = ['initial', 'extra']).first()
        
    if not challenge:
        response['error'] = 'Идентификатор соревнования указан неверно или у вас нет прав на сдачу данного соревнования'
    else:
        if not check_active_challenge(member_challenges, challenge):            
            response['error'] = 'Время на выполнение задачи истекло'
            response['member_amount'] = Member.objects.filter(user = request.user).first().amount_declension()
            
            response['task'] = model_to_dict(challenge.task)            
            response['task']['subtitle'] =  challenge.task.format_verbose() + "  " + challenge.task.cost_declension() \
                    + "  " + task_duration(challenge.task.duration_in_seconds())
            response['task']['completed'] = "Уже " + declension(challenge.task.completed, "сделал", "", "и", "и") + " " \
                + str(challenge.task.completed) + declension(challenge.task.completed, " человек", "", "а", "")                    
            time_delta = challenge.time_limit - timezone.now()                    
            response['task']['remained_time'] = 0
            response['task']['member_lost'] = challenge.task.cost
            response['challenge'] = {'id': challenge.id}
        else:
            if not request.POST.get("description") or not request.POST.get("title") \
            or not request.POST.get("link") or not request.POST.get("link-title") or \
            len(request.FILES) == 0:
                response['error'] = 'Не все поля заполнены'
            else:                
                file = request.FILES['file']
                file_type = file.content_type.split('/')[0]
                                
                if len(file.name.split('.')) == 1 or file_type not in settings.MATERIAL_UPLOAD_FILE_TYPES:
                    response['error'] = 'Данный тип файла не поддерживается'
                                    
                if  file._size > settings.MATERIAL_UPLOAD_FILE_MAX_SIZE:
                    response['error'] = 'Файл слишком большой'        
            
                if 'error' not in response:
                                    
                    # Помечаем задачу выполненной и добавляем историю
                    challenge.status = 'wm'
                    challenge.save()
                    
                    result = Result(
                        task        = challenge.task,
                        member      = challenge.member,
                        challenge   = challenge,
                        description = request.POST.get("description"),
                        url         = request.POST.get("link"),
                        title       = request.POST.get("title"),
                        url_title   = request.POST.get("link-title"),
                        image       = request.FILES['file']
                    )                    
                    result.save()

                    # Добавляем событие
                    challenge_history = ChallengeHistory(challenge = challenge, status = 'wm',
                        description = 'Пользователь сдал материалы. Создан Результат №' + str(result.id))
                    challenge_history.save()
                
                    response = {'status': 1, 'result_id': result.id }
                    
                    # уведомляем модераторов о новом challenge                    
                    if settings.MULTICUP['NOTIFICATIONS']['CHALLENGE_CREATE']:
                        group = Group.objects.get(name = 'Moderator')
                        moderators = group.user_set.all()
                        
                        emails = [moderator.email for moderator in moderators]
                        emails.append(settings.EMAIL_HOST_USER)
                        title = 'Новый материал на проверку'
                        message = render_to_string('email/materials-for-check.html', {
                            'server_url'    : settings.SERVER_URL,
                            'challenge_id'  : challenge.id,
                            'title'         : title                
                        })
                
                        send_mail(
                            title,
                            message,
                            settings.EMAIL_HOST_USER,
                            emails,
                            html_message = message,                        
                        )
                            
    response = json.dumps(response, ensure_ascii=False)
    return HttpResponse(response, content_type="application/json; encoding=utf-8")


# Оспорить результат модерации
def contest(request, challenge_pk):
    
    response = {'status': 0}
        
    if not request.POST.get('comment') or len(request.POST.get('comment')) < 20:
        response['error'] = 'Поле комментарий обязательно! (минимум 20 символов)'
    else:        
        if not request.user.is_authenticated():
            response['error'] = 'Пользователь не авторизован'
        else:
            challenge = Challenge.objects.filter(pk = challenge_pk, member__user = request.user).first()
            
            if challenge and challenge.status in ['decline', 'wom', 'wm']:
                if settings.MULTICUP['NOTIFICATIONS']['CHALLENGE_CONTEST']:
                    group = Group.objects.get(name = 'Moderator')
                    moderators = group.user_set.all()     
                    emails = [m.email for m in moderators]
                    emails.append(settings.EMAIL_HOST_USER)
                    
                    email_context = {
                        'server_url'    : settings.SERVER_URL,
                        'challenge_id'  : challenge.id,
                        'title'         : 'Участник открыл спор по отклоненному материалу',
                        'bio'           : "{0} {1}".format(challenge.member.user.first_name, challenge.member.user.last_name),
                        'email'         : challenge.member.user.email,
                        'comment'       : request.POST.get('comment')
                    }

                    message = render_to_string('email/member-open-contest.html', email_context)
                                
                    send_mail(
                        email_context['title'],
                        message,
                        settings.EMAIL_HOST_USER,
                        emails,
                        html_message = message,
                    )

                response = {'status': 1}    
            else:
                response['error'] = 'У вас нет к доступа к соревнованию или его нельзя оспорить'
                
    response = json.dumps(response, ensure_ascii = False)
    return HttpResponse(response, content_type = "application/json; encoding=utf-8")

#   Проверка и пересчет баллоы за голосования жюри, запись изменений в историю баллов участника
#       @param challenge_id - ID challenge
#       @return None
#       @test challenges.test.test_check_amount
def checkAmounts(challenge_id):

    challenge = Challenge.objects.filter(id = challenge_id).first()    
    if not challenge:
        return
        
    resultvotes = ResultVote.objects.filter(result__challenge = challenge, voted = True)
    average_vote = 0.0
    for resultvote in resultvotes:    
        average_vote += resultvote.average_vote
    previous_vote_amount = challenge.vote_amount
    if (resultvotes.count() > 0):
        challenge.vote_amount = int((average_vote / resultvotes.count()) * challenge.task.cost / 10)
    else:
        challenge.vote_amount = 0
    challenge.total_amount = challenge.total_amount + challenge.vote_amount - previous_vote_amount
    challenge.save()
    
    if previous_vote_amount > challenge.vote_amount:
        action = 'remove'
        challenge.member.message = 'Вы получили новую оценку жюри и потеряли {0} баллов'.format(previous_vote_amount - challenge.vote_amount)
    else:
        action = 'add'
        challenge.member.message = 'Вы получили новую оценку жюри и получили {0} баллов'.format(challenge.vote_amount - previous_vote_amount)
        
    challenge.member.amount = challenge.member.amount + challenge.vote_amount - previous_vote_amount    
    check_rating_result = check_member_rating(challenge.member)    
    challenge.member.save()
            
    MemberAmountHistory.objects.create(
        member = challenge.member,
        action = action,
        description = challenge.member.message
    )