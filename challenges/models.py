from django.db import models
from front.templatetags.multicup import declension
from members.models import Member
from tasks.models import Task

STATUS_CHOICES = (        
        ('initial', 'Выполняется'),                          
        ('Сдано участником', (            
                ('wm', 'Сдано (с материалами)'),
                ('wom', 'Не успел по времени'),
            )
        ),
        ('Проверено модератором', (            
                ('accepted', 'Принято'),
                ('decline', 'Отклонено модератором'),
                ('extra', 'Добавлено время'),
            )
        ),
        ('scored', 'Оценено жюри'),
    )

class Challenge(models.Model):
    class Meta:
        verbose_name = 'Соревнование'
        verbose_name_plural = 'Соревнования'
    
    task = models.ForeignKey(Task, on_delete = models.CASCADE, verbose_name = "Задание")
    member = models.ForeignKey(Member, on_delete = models.CASCADE, verbose_name = "Участник")        
    status = models.CharField(max_length = 20, choices = STATUS_CHOICES, verbose_name = "Статус")
    vote_amount = models.IntegerField(default = 0, verbose_name = "Баллы за оценки жюри")
    likes_amount = models.IntegerField(default = 0, verbose_name = "Баллы за лайки")
    total_amount = models.IntegerField(default = 0, verbose_name = "Всего баллов")
    started_at = models.DateTimeField(auto_now_add = True, blank = True, verbose_name = "Начато")
    time_limit = models.DateTimeField(blank = True, verbose_name = "Лимит по времени")
    notified = models.BooleanField(default = False, verbose_name = "Было ли напоминание")
    
    def total_amount_declension(self):
        return str(self.total_amount) + " " + declension(self.total_amount, "балл", "", "а", "ов")

    def status_verbose(self):                
        for k, v in STATUS_CHOICES:
                if isinstance(v, tuple):
                        for k1, v1 in v:
                                if k1 == self.status:
                                        return v1
                if k == self.status:
                    return v            
        return ''
    
class ChallengeHistory(models.Model):
    class Meta:
        verbose_name = 'История соревнования'
        verbose_name_plural = 'История соревнований'
    
    challenge = models.ForeignKey(Challenge, on_delete = models.CASCADE, verbose_name = "Соревнование")
    status = models.CharField(max_length = 20, choices = STATUS_CHOICES, verbose_name = "Статус")
    description = models.CharField(max_length = 1024, blank = True, verbose_name = "Описание")
    changed_at = models.DateTimeField(auto_now_add = True, blank = True, verbose_name = "Изменено")