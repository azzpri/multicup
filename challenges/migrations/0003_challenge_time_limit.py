# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-21 19:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('challenges', '0002_auto_20161021_2055'),
    ]

    operations = [
        migrations.AddField(
            model_name='challenge',
            name='time_limit',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, verbose_name='Лимит по времени'),
            preserve_default=False,
        ),
    ]
