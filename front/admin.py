import front
from .models import Content, Post, Blog, Setting
from ckeditor.widgets import CKEditorWidget
from django import forms
from django.contrib import admin
from django.utils.html import strip_tags
from html import unescape

class ContentAdminForm(forms.ModelForm):
    attributes = forms.CharField(widget = CKEditorWidget(), required = False)
    
    class Meta:
        model = Content
        fields = '__all__'        
        
class ContentAdmin(admin.ModelAdmin):
    list_display = ('title',)    
    form = ContentAdminForm
    
    def save_model(self, request, obj, form, change):        
        if obj.type != 'html':            
            obj.attributes = strip_tags(unescape(obj.attributes))            
        obj.save()
    
    
class PostAdminForm(forms.ModelForm):
    full_description = forms.CharField(widget=CKEditorWidget(), label="Полное описание")
    
    class Meta:
        model = Post
        fields = '__all__'    

class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'url_name')
    form = PostAdminForm
    
class BlogAdmin(admin.ModelAdmin):
    list_display = ('type', 'order', 'record_title')
    list_editable = ('order',)
    list_filter = ('type',)
    
    def record_title(self, obj):
        if obj.post:
            return obj.post.title
        if obj.material:
            return obj.material.title
        if obj.member:
            return str(obj.member.user.first_name) + " " + str(obj.member.user.last_name)
        if obj.juror:
            return str(obj.juror.member.user.first_name) + " " + str(obj.juror.member.user.last_name)
    
    record_title.short_description = 'Название'    

class SettingAdmin(admin.ModelAdmin):
    list_display = ('key', 'value', 'title')
    
    def save_model(self, request, obj, form, change):                    
        if obj.key in front.views.CUP_DATES:
            front.views.CUP_DATES[obj.key] = obj.value_as_date()
        obj.save()
    
admin.site.register(Content, ContentAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Blog, BlogAdmin)
admin.site.register(Setting, SettingAdmin)