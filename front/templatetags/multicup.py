import front
from django import template
from django.contrib.auth.models import Group 
from django.utils.safestring import mark_safe
from django.shortcuts import render
from django.template.loader import render_to_string

register = template.Library()

#   Склонение числительных
#   {% declension value "балл" '' 'а' 'ов' %}
@register.simple_tag
def declension(value, name, one, few, many):
    
    if not value or isinstance(value, str):
        value = 0
        
    i = value % 10
    j = value % 100
    if i == 1 and j != 11:
        return name + one
    elif i in range(2,5) and j not in range(12, 15):
        return name + few
    else:
        return name + many
    
#   Слайдер 
#   {% slider <data> <template> %}, напр. {% slider materials 'materials' %}
@register.simple_tag
def slider(data, name):
    content = render_to_string('slider/' + name + ".html", {'data' : data[:9]})
    response = render_to_string('slider/slider.html', {'data' : data, 'slider_content': content, 'name' : name})
    return mark_safe(response)

#   Содержимое слайдера
#   {% slider_content <data> <template> %}, напр. {% slider_content materials 'materials' %}
@register.simple_tag
def slider_content(data, name):
    response = render_to_string('slider/' + name + ".html", {'data' : data})
    return mark_safe(response)

#   Отображение колонки с ссылками в footer .navigation
#   {% footer_column links %}
@register.simple_tag
def footer_column(links):    
    response = '<div>'    
    for link in links:
        response += '<a href="'+ link['url'] +'">' + link['title'] + '</a>'
    response += '</div>'
        
    return mark_safe(response)
            
# Конвертируем секунды в формат "X Y", где X - количество, Y - секунд, минут, часов, дней
#   @duration - секунд (int)
@register.filter
def task_duration(duration):
    
    if duration <= 60:        
        return str(duration) + " " + declension(duration, "секунд", "a", "ы", "")
    else:
        if duration < 3600:
            dm = divmod(duration, 60)
            return str(dm[0]) + " " + declension(dm[0], "минут", "а", "ы", "")
        else:
            duration = divmod(duration, 3600)[0]
            ans = ""
            dm = divmod(duration, 24)
            
            if dm[0] > 0:            
                ans = str(dm[0]) + " " + declension(dm[0], "д", "ень", "ня", "ней")
            
            if dm[1] > 0:
                if len(ans) > 0:
                    ans += " "
                ans += str(dm[1]) + " " + declension(dm[1], "час", "", "а", "ов")
            
            return ans

# Преобразовать ссылки пользователя в html 
#   {% member_links member %}
@register.simple_tag
def member_links(member):
    response = ""
    if member:
        if member.project and len(member.project) > 0:
            response += '<a href="' + member.project + '">' + member.project.replace("http://", '') + '</a>'
        if member.socials and len(member.socials) > 0:
            for link in member.socials.split('\n'):
                if len(link) > 0:
                    if link.startswith(('https://', 'http://')):
                        response += '<a href="' + link + '">' + link.replace("http://", '').replace("https://", '') + '</a>'
                    else:
                        response += '<a href="https://' + link + '">' + link.replace("http://", '').replace("https://", '') + '</a>'
    return mark_safe(response)

# Показываем профиль пользователя
#   {% member_profile member_id %}
@register.simple_tag
def member_profile(member_id):     
    return mark_safe(front.views.member_profile(None, member_id, True))

# Проверка групп пользователя
@register.filter(name='has_group') 
def has_group(user, group_name):
    group =  Group.objects.get(name = group_name) 
    return group in user.groups.all() 