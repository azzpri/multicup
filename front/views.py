import datetime, json, re
from .models import Content, Blog, Post, Setting
from .templatetags.multicup import declension, slider_content as slider_content_tag, slider as slider_template_tag, has_group
from challenges.models import Challenge
from challenges.views import challenge_to_material
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import logout as auth_logout, login as auth_login, load_backend
from django.contrib.auth.models import User
from django.db.models import Q
from django.forms.models import model_to_dict
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.template.context import RequestContext
from django.template.loader import render_to_string
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from members.models import Member, Juror
from multicup import settings
from results.models import ResultVote, Result, get_materials_for_help
from social.apps.django_app.utils import psa
from social.backends.oauth import BaseOAuth1, BaseOAuth2
from social.backends.vk import VKOAuth2
from social.backends.utils import load_backends
from tasks.models import Task, FORMAT_CHOICES

# Получаем даты проведения конкурса и храним их в переменных
dates = Setting.objects.all()
CUP_DATES = {
    'challenge_add_start' : False,
    'challenge_add_finish' : False,
    'challenge_complete_finish' : False,
    'rating_change_finish' : False
}

for k, v in CUP_DATES.items():
    date = dates.filter(key = k).first()
    if date:
        CUP_DATES[k] = date.value_as_date()

# функция рендера страницы, добавляющая необходимые параметры
def base_render(request, page, dict1, content_places = None):
    
    places = ['meta', 'header', 'footer']
    
    dict1['facebook_app_id'] = settings.SOCIAL_AUTH_FACEBOOK_KEY
    
    if content_places: places.extend(content_places)
    
    # Получаем содержимое сайта
    content = Content.objects.filter(place__in = places).values('title', 'place', 'attributes', 'type')
    content = list(content)
    
    for c in content:
        if len(c['attributes']) == 0 or c['place'] in dict1:
            continue
        
        if c['type'] == 'json':
            cleared = re.sub(r'\r\n','', c['attributes'])
            
            dict1[c['place']] = json.loads(cleared)
            try:
                dict1[c['place']] = json.loads(cleared)
            except Exception:
                pass
        else:
            dict1[c['place']] = c['attributes']
        
    # Статистика пользователя
    if request.user.is_authenticated:
        member = Member.objects.filter(user = request.user).first()        
        if member:
            if not member.city:
                dict1['message'] = {'text': 'Пожалуйста, заполните свой профиль, чтобы продолжить участие в конкурсе', 'auth' : True}
            
            if len(member.message) > 0:            
                dict1['message'] = {
                    'text': member.message,
                    'link' : {
                        'url' : '/about-rating',
                        'title' : 'Как работает рейтинг',
                    },
                    'close' : True
                }
        dict1['member'] = member
    
    return render(request, page, dict1)

# Главная страница
def index(request):    
    jurors_ids = [ j for j in Juror.objects.all().values_list('member__id', flat = True)]
    members = Member.objects.exclude(Q(id__in = jurors_ids) | Q(city = '')).count()
    
    cities = Member.objects.exclude(id__in = jurors_ids).values('city').distinct().count()
    results = Result.objects.count()
    
    blog_records = list(Blog.objects.all().order_by('order'))
    materials = [challenge_to_material(r.challenge) for r in Result.objects.filter(challenge__status = 'accepted').order_by('-created_at')]
        
    return base_render(request, 'home.html',
        dict({
            'available_backends': load_backends(settings.AUTHENTICATION_BACKENDS),            
            'members'           : members,
            'cities'            : cities,
            'results'           : results,
            'materials'         : materials,            
            'blog_records'      : blog_records,   
        }),
        ['home_description_title', 'home_description_content', 'home_prizes', 'home_partners']
    )

# Авторизация
def login(request):
    return base_render(request, 'login.html',
        dict({'available_backends': load_backends(settings.AUTHENTICATION_BACKENDS)})
    )

# Выход из системы
def logout(request):
    auth_logout(request)
    return redirect('/')

# Получение слайдеров
@csrf_exempt
def slider_content(request):
    
    response = {}
    if not request.POST.get('slider') or not request.POST.get('start') \
    or not request.POST.get('end'):
        response['error'] = 'Неверно заполнены данные'
    else:
        slider = request.POST.get('slider')
        start = int(request.POST.get('start'))
        end = int(request.POST.get('end'))
        if slider == 'blog':
            result = Blog.objects.all().order_by('order')[start:end]
        elif slider == 'materials':
            result = [challenge_to_material(r.challenge) for r in Result.objects.filter(challenge__status = 'accepted')[start:end]]
        
        response['length'] = len(result)
        
        if response['length'] > 0:
            response['result'] = slider_content_tag(list(result)[:9], slider)
        else:
            response['result'] = ''
        
    response = json.dumps(response, ensure_ascii=False)
    return HttpResponse(response, content_type="application/json; encoding=utf-8")

# Задания
def tasks(request):
    
    member_tasks_id = materials_for_vote = materials_for_help = materials_for_moderation = []
    member = False
    user_is_moderator = False
    if request.user.is_authenticated():
        member = Member.objects.filter(user_id = request.user.id).first()
        if member:
            member_tasks_id = Challenge.objects.filter(member = member).values_list('task__id', flat = True)
            
            if member.is_juror():
                materials_for_vote = [challenge_to_material(x) for x in Challenge.objects.filter(
                    id__in = list(ResultVote.objects.filter(juror = member.juror, voted = False) 
                        .values_list('result__challenge_id', flat = True)))]
                                
                materials_for_help = get_materials_for_help(member.juror)
            
            if has_group(request.user, 'Moderator'):
                user_is_moderator = True
                materials_for_moderation = [challenge_to_material(x) for x in Challenge.objects.filter(status = 'wm')]
        
    tasks = Task.objects.exclude(id__in = member_tasks_id)
    formats = FORMAT_CHOICES
    costs = list(tasks.values_list('cost', flat = True).order_by('cost').distinct())
    
    return base_render(request, 'tasks.html', dict({
        'costs'                     : costs,
        'formats'                   : formats,
        'tasks'                     : tasks,
        'member'                    : member,
        'materials_for_vote'        : materials_for_vote,
        'materials_for_help'        : materials_for_help,
        'materials_for_moderation'  : materials_for_moderation,
        'user_is_moderator'         : user_is_moderator,
    }),[])

# Слайдер материалов для задачи
#   task_id - ID задачи
#   type - тип материалов
#       'unchecked' - непроверенные материалы (только для жюри)
#       default - проверенные материалы

@csrf_exempt
def task_materials(request, task_id, type = None):
    response = {}
        
    if request.user.is_authenticated():
        member = Member.objects.filter(user = request.user).first()
        
        if member and member.is_juror():
            juror = Juror.objects.filter(member = member).first()
            if type == 'unchecked':
                materials = []
                results = Result.objects.filter(challenge__status = 'accepted', task__id = task_id)
                if results.first() and juror:
                    resultvote = ResultVote.objects.filter(result = results.first(), juror = juror).first()                    
                    if resultvote and not resultvote.voted:
                        materials = results
            else:   
                materials = Result.objects.filter(challenge__status = 'voted', task__id = task_id)
            
    if not 'materials' in locals():
        materials = Result.objects.filter(challenge__status__in = ['voted', 'accepted'], task__id = task_id)
        
    response['result'] = slider_template_tag([challenge_to_material(m.challenge) for m in materials], 'materials')
    response = json.dumps(response, ensure_ascii=False)
    
    return HttpResponse(response, content_type="application/json; encoding=utf-8")

# Материалы /materials/
def materials(request, challenge_id = None):
    
    result_set = Result.objects.filter(challenge__status = 'accepted').order_by('-challenge__total_amount')
    results = []
    exclude_result_ids = []
    for result in result_set:
        add = True
        for r in results:            
            if result.challenge.id == r.challenge.id:
                if result.created_at > r.created_at:
                    exclude_result_ids.append(r.id)
                else:
                    add = False
        if add:
            results.append(result)
    
    result_set = result_set.exclude(id__in = exclude_result_ids)        
    user_is_moderator = has_group(request.user, 'Moderator')
    
    current_result = None
    if challenge_id:
        current_result = result_set.filter(challenge__id = challenge_id).first()
    
    best_results = result_set.filter(created_at__gt = timezone.now() - datetime.timedelta(days = 7))[:3]
    best_materials = [challenge_to_material(r.challenge) for r in best_results]
        
    all_result_set = result_set.exclude(id__in = list(best_results.values_list('id', flat = True))).order_by('created_at')
    all_materials = []
    
    if request.user.is_authenticated():
        member = Member.objects.filter(user_id = request.user.id).first()
        juror = Juror.objects.filter(member__user__id = request.user.id).first()
        
        resultvotes = {}
        for vote in ResultVote.objects.order_by('result__challenge__id', 'result__id'):            
            if not vote.result.challenge.id in resultvotes:
                resultvotes[vote.result.challenge.id] = []
            resultvotes[vote.result.challenge.id].append(vote)
            
        try:
            result_vote_limit = int(Setting.objects.get(key = 'result_vote_limit').value)
        except:
            result_vote_limit = 1
        
    for result in all_result_set:        
        can_add = True
        
        for m in all_materials:
            if m['challenge'].id == result.challenge.id:
                can_add = False
                break
            
        if can_add:
            material = challenge_to_material(result.challenge)
            if 'member' in locals():
                if not juror:                    
                    if result.challenge.member == member: 
                        material['mine'] = 1                
                elif result.challenge.id in resultvotes:
                    votes = resultvotes[result.challenge.id]
                    
                    #   Проверяем, есть ли этот оценка для этого материала от жюри,
                    #   и выставляем флаг "На проверку" или "Моя оценка"        
                    for vote in votes:                        
                        if vote.juror == juror:
                            if vote.voted:
                                material['mine'] = 1
                            else:
                                material['mine'] = 2
                            break
                    
                    #   Выставляем флаг помощь, если флагов у материала нет, голосований меньше лимита и
                    #   категории жюри позоволяют голосовать за данный материал
                    if material['mine'] == 0:
                        if len(votes) < result_vote_limit and result.challenge.task.format in juror.formats:
                            material['mine'] = 3
                                        
            all_materials.append(material)
    
    if user_is_moderator:        
        all_materials.extend([challenge_to_material(m) for m in Challenge.objects.exclude(status__in = ['accepted', 'initial', 'extra'])])
    
    costs = list(all_result_set.values_list('challenge__task__cost', flat = True).order_by('challenge__task__cost').distinct())
    response_dict = {
        'best_materials'    : best_materials,
        'all_materials'     : all_materials,
        'costs'             : costs,
        'material'          : challenge_id,
        'user_is_moderator' : user_is_moderator
    }
    
    if current_result:
        response_dict['meta'] = {
            "og_url"           : "{0}materials/{1}".format(settings.SERVER_URL, challenge_id),
            "og_type"          : "article",
            "og_title"          : "КУБОК МУЛЬТИМЕДИЙНОГО ВЫЗОВА",            
            "og_image"         : "{0}media/{1}".format(settings.SERVER_URL, current_result.image.name), 
            "og_description"   : current_result.description or ""
        }
    return base_render(request, 'materials.html', dict(response_dict),[])

# Метод возвращает контент для блоков Неопубликованные и Опубликованные материалы
# Неопубликованные - wom, decline
# Опубликованные - accepted, scored
def member_unpublished_materials(request, strip = False):
        
    member_materials = Challenge.objects.all().filter(member__user = request.user).order_by('-started_at')
        
    if strip:
        member_unpublished_materials = []        
        if member_materials.first():
            member_unpublished_materials = [challenge_to_material(m) for m in
                member_materials.filter(status__in = ['wom', 'decline']).exclude(id = member_materials.first().id)]
    else:    
        member_unpublished_materials = [challenge_to_material(m) for m in member_materials.filter(status__in = ['wom', 'decline'])]

    member_published_materials = [challenge_to_material(m) for m in member_materials.filter(status__in = ['accepted', 'scored'])]

    return {
        'published_materials'   : slider_content_tag(member_published_materials, 'materials'),
        'unpublished_materials' : slider_content_tag(member_unpublished_materials, 'materials')
    }


#   Пользователи
def members(request):
    return base_render(request, 'members.html', dict({        
    }),[])

#   После регистрации пользователя проверяем, заполнен ли у него профиль.
#   Если да, то отправляем на главную.
def after_login(request):
    
    if request.user.is_authenticated:
        member = Member.objects.filter(user = request.user).first()
        
        if member and len(member.city) == 0:
            return redirect('/members/?profile=1&welcome=1');
        
    return redirect('/');
    
    

# Карточка пользователя
def member_profile(request, member_id, from_tag = False):
    
    juror = Juror.objects.filter(member__id = member_id).first()
    member_materials = []    
    if juror:
        results = ResultVote.objects.filter(juror = juror, voted = True)
        member_materials = [challenge_to_material(rv.result.challenge) for rv in results]
        member = juror.member
    else:        
        included_status = ['accepted','voted']
        
        member = Member.objects.filter(id = member_id).first()
        
        if (member and request and member.user == request.user) or from_tag:
            included_status += ['decline', 'initial', 'wm', 'extra']
            
        challenges = Challenge.objects.filter(member = member, status__in = included_status)
        member_materials = [challenge_to_material(c) for c in challenges]
        
    response = render_to_string('parts/profile.html', {
        'member_materials'  : member_materials,
        'displayed_member'  : member,
        'from_tag'          : from_tag
    })
    if request:
        return HttpResponse(response, content_type="application/json; encoding=utf-8")
    else:
        return response
    
#   Жюри
def jurors(request):
    jurors = Juror.objects.all()
    return base_render(request, 'jurors.html', dict({
        'jurors' : jurors
    }),[])

# Отображение статических страниц и записей блога
def page(request, page_key):
    
    try:
        page_key = int(page_key)
    except Exception:
        page = Post.objects.filter(url_name = page_key).first()
    else:
        page = Post.objects.filter(id = page_key).first()
        
    return base_render(request, 'page.html', dict({
        'page' : page
    }),[])

#   Аутентификация от имени пользователя для администраторов
#
@staff_member_required
def test_auth(request, user_id):
    
    if request.user.is_authenticated:        
        user = User.objects.filter(id = user_id).first()

        if user:
            auth_login(request, user, 'django.contrib.auth.backends.ModelBackend')
            
            return HttpResponse('Ок')
    
    return HttpResponse('Что-то не так')

# Отображение статической страницы О конкурсе
def about(request):
    return base_render(request, 'about.html',dict())