import re, json, unicodedata
from django.db import models
from django.utils.dateparse import parse_datetime
from members.models import Member, Juror
from results.models import Result

# Содержимое сайта
class Content(models.Model):
    class Meta:
        verbose_name = 'Контент сайта'
        verbose_name_plural = 'Контент сайта'
    
    TYPE_CHOICES = (        
        ('plain', 'Простой текст'),
        ('html', 'HTML'),
        ('json', 'JSON'),
    )
    
    title = models.CharField(max_length=50, verbose_name="Название")    
    place = models.CharField(max_length=30, verbose_name="Размещение")
    type = models.CharField(max_length=20, choices=TYPE_CHOICES, default='plain', verbose_name="Тип")
    attributes = models.TextField(verbose_name = "Атрибуты", blank = True)

# Сообщения для блога / страница
class Post(models.Model):
    class Meta:
        verbose_name = 'Запись блога и статическая страница'
        verbose_name_plural = 'Записи блога и статические страницы'
        
    title = models.CharField(max_length=200, verbose_name="Название")
    short_description = models.CharField(max_length=500, verbose_name="Краткое описание", blank = True, help_text="Будет использоваться в превью записи на главной")
    full_description = models.TextField(verbose_name="Полное описание")
    url_name = models.CharField(verbose_name="URL название", blank = True, help_text='Принимает латинские буквы, цифры, "-", напр. "page-good1"', max_length=20)
    image = models.FileField(upload_to='uploads/posts/', verbose_name="Изображение", blank = True, help_text="Будет использоваться в превью записи на главной")
    
    def __str__(self):
        return self.title
    
BLOG_TYPE = (
    ('post', 'Пост'),
    ('material', 'Материал'),
    ('juror', 'Жюри'),
    ('member', 'Участник'),
)

# Записи для отображения в блоке Блог на главной странице 
class Blog(models.Model):
    class Meta:
        verbose_name = 'Содержание блога'
        verbose_name_plural = 'Содержание блога'
        
    type = models.CharField(max_length=20, choices=BLOG_TYPE, default='post', verbose_name="Тип")
    post = models.ForeignKey(Post, null=True, blank=True, on_delete=models.CASCADE, verbose_name="Пост")
    material = models.ForeignKey(Result, null=True, blank=True, on_delete=models.CASCADE, verbose_name="Материал")
    member = models.ForeignKey(Member, null=True, blank=True, on_delete=models.CASCADE, verbose_name="Участник")
    juror = models.ForeignKey(Juror, null=True, blank=True, on_delete=models.CASCADE, verbose_name="Жюри")
    order = models.IntegerField(default=0, verbose_name="Порядок")

# Общие настройки
class Setting(models.Model):
    class Meta:
        verbose_name = 'Настройка'
        verbose_name_plural = 'Настройки'
            
    key = models.CharField(max_length = 30, verbose_name = "Ключ")
    value = models.CharField(max_length = 50, verbose_name = "Значение")
    title = models.CharField(max_length = 100, verbose_name = "Название", blank = True)
    
    def value_as_date(self):
        if self.value:
            new_str = unicodedata.normalize("NFKD", self.value)
            return parse_datetime(new_str)