from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^ajax/slider-content/$', views.slider_content),
    url(r'^ajax/task-materials-slider/(?P<task_id>[0-9]+)$', views.task_materials),
    url(r'^ajax/task-unchecked-slider/(?P<task_id>[0-9]+)$', views.task_materials, {'type': 'unchecked'}),
    url(r'^ajax/view-profile/(?P<member_id>[0-9]+)$', views.member_profile),
    url(r'^signin/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^$', views.index, name='index'),
    url(r'^tasks/$', views.tasks, name='tasks'),
    url(r'^materials/$', views.materials, name='materials'),
    url(r'^materials/(?P<challenge_id>[0-9]+)$', views.materials),
    url(r'^members/after-login/$', views.after_login),
    url(r'^members/$', views.members, name='members'),    
    url(r'^jurors/$', views.jurors, name='jurors'),
    url(r'^page/(?P<page_key>[\w-]+)/$', views.page, name='page'),
    url(r'^test/auth/(?P<user_id>[0-9]+)$', views.test_auth),
    url(r'^about/$', views.about),
]

