from django.conf.urls import url

from . import views

urlpatterns = [
    #url(r'^profile/view/(?P<member_pk>[0-9]+)$', views.view_profile, name='view-profile'),    
    url(r'^rating/district/$', views.geo_rating, {'field': 'district'}, name='district-rating'),
    url(r'^rating/region/$', views.geo_rating, {'field': 'region'}, name='region-rating'),
    url(r'^rating/city/$', views.geo_rating, {'field': 'city'}, name='city-rating'),
    url(r'^rating/$', views.rating, name='rating'),
    url(r'^edit-profile/$', views.edit_profile, name='edit-profile'),
    url(r'^clear-message/$', views.clear_message),
]