import datetime, json, sys
from challenges import views
from challenges.models import Challenge
from django.utils import timezone
from django.test import TestCase, override_settings
from django.test.client import Client
from django.contrib.auth.models import User, Group
from django.core import mail, management
from members.models import Member, Juror, MemberAmountHistory
from multicup.settings import SERVER_URL, EMAIL_HOST_USER
from results.models import Result, ResultVote, ResultLikeHistory
from tasks.models import Task

class MembersTestCase(TestCase):
    def setUp(self):                   
        task1 = Task.objects.create(
                title = "Тестовая задача 1",
                short_description = "Короткое описание тестовой задачи",
                full_description = "Полное описание тестовой задачи",
                cost = 300,
                completed = 0,
                average_vote = 0,
                duration = 20
            )
        
        user1 = User.objects.create(username = 'testmember1')
        member1 = Member.objects.create(user = user1, amount = 300)
        
        challenge1 = Challenge.objects.create(
            status = 'wm',
            member = member1,
            task = task1,
            time_limit = '2017-12-30 12:30:28'
        )       
        
        result1 = Result.objects.create(
            task = task1,
            challenge = challenge1,
            member = member1,
            description = 'Описание результата',
            image = 'test.png',
            title = 'Название результата',
            url = 'Ссылка',
            url_title = 'Название cсылки'            
        )
        
        user2 = User.objects.create(username = 'testmember2')
        member2 = Member.objects.create(user = user2, amount = 300)
        
        challenge2 = Challenge.objects.create(
            status = 'wm',
            member = member2,
            task = task1,
            time_limit = '2017-12-30 12:30:28'
        )       
        
        result2 = Result.objects.create(
            task = task1,
            challenge = challenge2,
            member = member2,
            description = 'Описание результата 2',
            image = 'test.png',
            title = 'Название результата 2',
            url = 'Ссылка 2',
            url_title = 'Название cсылки 2'
        )
    
    # Проверяем пересчет рейтинга
    def test_managment_recounrating(self):
        
        member1 = Member.objects.get(user__username = 'testmember1')
        challenge1 = Challenge.objects.get(task__title = 'Тестовая задача 1', member = member1)
        result1 = Result.objects.filter(challenge = challenge1).first()
        
        self.assertEqual(member1.amount, 300)
        self.assertEqual(challenge1.total_amount, 0)
        self.assertEqual(challenge1.likes_amount, 0)
        
        like1 = ResultLikeHistory.objects.create(result = result1, user = 'testlike1').save()
        result1.likes += 1
        result1.save()
                
        management.call_command('recountrating')
        
        member1 = Member.objects.get(user__username = 'testmember1')
        challenge1 = Challenge.objects.get(task__title = 'Тестовая задача 1', member = member1)
        self.assertEqual(member1.amount, 600)
        self.assertEqual(challenge1.total_amount, 300)
        self.assertEqual(challenge1.likes_amount, 300)
    
        member2 = Member.objects.get(user__username = 'testmember2')
        challenge2 = Challenge.objects.filter(task__title = 'Тестовая задача 1', member = member2).first()
        result2 = Result.objects.filter(challenge = challenge2).first()
        
        like2 = ResultLikeHistory.objects.create(result = result2, user = 'testlike2').save()
        result2.likes += 1
        result2.save()
                
        management.call_command('recountrating')
        
        member2 = Member.objects.get(user__username = 'testmember2')
        challenge2 = Challenge.objects.get(task__title = 'Тестовая задача 1', member = member2)
        self.assertEqual(member2.amount, 600)
        self.assertEqual(challenge1.total_amount, 300)
        self.assertEqual(challenge1.likes_amount, 300)
        
        result2 = Result.objects.filter(challenge = challenge2).first()
        like3 = ResultLikeHistory.objects.create(result = result2, user = 'testlike3').save()
        result2.likes += 1
        result2.save()
        
        management.call_command('recountrating')
        
        member1 = Member.objects.get(user__username = 'testmember1')
        challenge1 = Challenge.objects.get(task__title = 'Тестовая задача 1', member = member1)
        self.assertEqual(member1.amount, 410)
        self.assertEqual(challenge1.total_amount, 110)
        self.assertEqual(challenge1.likes_amount, 110)
        
        member2 = Member.objects.get(user__username = 'testmember2')
        challenge2 = Challenge.objects.get(task__title = 'Тестовая задача 1', member = member2)
        self.assertEqual(member2.amount, 600)
        self.assertEqual(challenge2.total_amount, 300)
        self.assertEqual(challenge2.likes_amount, 300)
        
        history = MemberAmountHistory.objects.all()
        sys.stderr.write(repr(history.values('action', 'description')) + '\n')
        self.assertEqual(history.count(), 3)