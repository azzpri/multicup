from django.contrib import admin
from .models import Member, MemberAmountHistory, Juror

class MemberAdmin(admin.ModelAdmin):
    list_display = ('bio','amount','rating','likes','challenge_completed')
    readonly_fields = ('geoobject','position','city','region','district', 'district_osmid', 'message')    
    search_fields = ['user__first_name', 'user__last_name', 'user__username']
    
    def bio(self, obj):
        return obj.user.first_name + " " + obj.user.last_name + " (" + obj.user.username + ")"
    
    bio.short_description = 'Пользователь'
    
class MemberAmountHistoryAdmin(admin.ModelAdmin):
    list_display = ('username', 'action', 'description', 'changed_at')
    search_fields = ['member__user__first_name', 'member__user__last_name', 'member__user__username']
    
    def username(self, obj):
        return obj.member.user.first_name + " " + obj.member.user.last_name + " (" + obj.member.user.username + ")"
    
    username.short_description = 'Пользователь'

class JurorAdmin(admin.ModelAdmin):
    list_display = ('username','average_vote','amount',)
    readonly_fields = ('average_vote','amount')
    search_fields = ['member__user__first_name', 'member__user__last_name', 'member__user__username']
    
    def username(self, obj):
        return obj.member.user.first_name + " " + obj.member.user.last_name + " (" + obj.member.user.username + ")"
    
    username.short_description = 'Пользователь'
    
admin.site.register(Member, MemberAdmin)
admin.site.register(MemberAmountHistory, MemberAmountHistoryAdmin)
admin.site.register(Juror, JurorAdmin)