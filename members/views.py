import json
from challenges.models import Challenge
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.db.models import Sum, Count, Q
from django.forms.models import model_to_dict
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from front.templatetags.multicup import declension
from members.models import Member, Juror, Region
from multicup import settings
from results.models import ResultVote, Result
from tasks.models import Task

# Выбор жюри для решения задачи
def chooseJuror(challenge_pk):
    
    challenge = Challenge.objects.filter(pk = challenge_pk).first()
    if not challenge:
        return    
    
    task_format = challenge.task.format

    try:
        juror = Juror.objects.raw('select j.id from members_juror j left outer join (select juror_id, count(juror_id) as count \
            from results_resultvote group by juror_id) rv on j.id = rv.juror_id where formats like %s \
            or formats = "" order by count, formats desc limit 1', ["%" + task_format + "%"])[0]
    except:
        return {'status': 0, 'error': 'Должен быть зарегистирован хотя бы 1 жюри'}
    
    result = Result.objects.filter(challenge__pk = challenge_pk).last()
    
    if not result:
        return {'status': 0, 'error': 'Не существует Результата для данного Соревнования'}

    resultvote = ResultVote(result = result, juror = juror)
    resultvote.save()
        
    if settings.MULTICUP['NOTIFICATIONS']['CHALLENGE_VOTE']:        
        user = User.objects.filter(id = juror.member.user_id).first()

        title = 'Новый материал на оценку'
        message = render_to_string('email/materials-for-vote.html', {
            'server_url'    : settings.SERVER_URL,
            'challenge_id'  : challenge.id,
            'title'         : title                
        })

        send_mail(
            title,
            message,
            settings.EMAIL_HOST_USER,
            [user.email, settings.EMAIL_HOST_USER],
            html_message = message,                        
        )
    
    return {'status' : 1}
    

# Делаем пользователя социальных сетей участником и уведомляем по почте о регистрации
def linkSocialUserToMember(backend, user, response, *args, **kwargs):        
    member = Member.objects.filter(user_id = user.id).first()
    
    if not member:
        member = Member(user = user, amount = settings.BASE_AMOUNT)
        member.save()
                
        title = 'Вы стали участником конкурса «Кубок мультимедийного вызова»'
        message = render_to_string('email/confirm-registration.html', {
            'server_url'    : settings.SERVER_URL,
            'title'         : title
        })
        
        if user.email:
            send_mail(
                    title,
                    message,
                    settings.EMAIL_HOST_USER,
                    [user.email, settings.EMAIL_HOST_USER],
                    html_message = message,
            )

# Получение профиля пользователя
def view_profile(request, member_pk):
    
    response = {'status': 0}
    
    member = Member.objects.filter(pk=member_pk)
    
    # проверяем, есть ли такой участник
    if len(member) != 1:
        response['error'] = 'Участника не существует'
    else:
        member = member.first()        
        profile = model_to_dict(member, None, ['user','photo','id'])
        
        # проверяем, совпадает ли id Участника с id пользователя
        if member.user_id != request.user.pk:
            del profile['subscription']
            del profile['amount']            
            if member.photo:
                profile['photo'] = member.photo.url
            profile['is_member'] = False
        else:
            profile['is_member'] = True
            
        # проверяем, является ли пользователь Жюри
        juror = Juror.objects.filter(member=member)
        
        if len(juror) == 1:
            juror = juror.first()
            profile['juror_average_vote'] = juror.average_vote
            profile['juror_amount'] = juror.amount
            profile['is_juror'] = True        
            
        response = {'status':1, 'profile':profile} 
    
    return response

# Сохранение профиля пользователя
#   Обязательные поля
#     'age', 'last_name', 'project', 'first_name', 'company', 'job', 'summary'
#   Необязательные поля, если повторное заполнение
#     'yandex_city', 'yandex_position', 'yandex_geo', 'yandex_region',
def edit_profile(request):
    
    response = {'status': 0}
        
    if not request.user.is_authenticated:
        response['error'] = 'Пользователь должен быть авторизован'
    else:
        member = Member.objects.filter(user = request.user).first()
        
        if not member:
            response['error'] = 'Пользователь не является участником'
        else:                
            required_fields = ['age', 'last_name', 'project', 'first_name', 'company', 'job', 'summary', 'email']
            
            if not member.city:
                required_fields += ['yandex_city', 'yandex_position', 'yandex_geo', 'yandex_region']
                        
            post_keys = [key for key, value in request.POST.items()]
            for key in required_fields:
                if key not in post_keys:
                    form_not_valid = True        

            if 'form_not_valid' in locals() or (len(request.FILES) == 0 and not member.photo):
                response['error'] = 'Данные заполнены неверно, проверьте правильность заполнения данных'
            else:
                if len(request.FILES) == 1:
                    file = request.FILES['file']
                    file_type = file.content_type.split('/')[0]
                                                
                    if len(file.name.split('.')) == 1 or file_type not in settings.MATERIAL_UPLOAD_FILE_TYPES:
                        response['error'] = 'Данный тип файла не поддерживается'
                                                    
                    if  file._size > settings.MATERIAL_UPLOAD_FILE_MAX_SIZE:
                        response['error'] = 'Файл слишком большой'        

                if 'error' not in response:
                    if request.POST.get('yandex_region'):
                        if request.POST.get('yandex_city') == 'Москва':
                            region_name = 'Московская область'
                        elif request.POST.get('yandex_city') == 'Санкт-Петербург':
                            region_name = 'Ленинградская область'
                        elif request.POST.get('yandex_city') == 'Севастопль':
                            region_name = 'Республика Крым'
                        else:
                            region_name = request.POST.get('yandex_region')
                        
                        region = Region.objects.filter(district_title = region_name).first()
                        
                        if not region:
                            response['error'] = 'Не удалось найти округ для указанного региона {0}'.format(region_name)
                    
                    if 'error' not in response:                    
                        member.user.first_name = request.POST.get('first_name')
                        member.user.last_name = request.POST.get('last_name')
                        member.user.save()
                    
                        if 'region' in locals():
                            member.district = region.region_title
                            member.geoobject = request.POST.get('yandex_geo')
                            member.city = request.POST.get('yandex_city')
                            member.position = request.POST.get('yandex_position')                                                    
                            member.region = region_name
                        
                        if len(request.FILES) == 1:    
                            member.photo = request.FILES['file']

                        if request.POST.get('subscription'):
                            member.subscription = True
                            
                        if member.is_juror() and request.POST.get('experience'):
                            member.juror.experience = request.POST.get('experience')
                            member.juror.save()
                        
                        member.user.email = request.POST.get('email')
                        member.user.save()
                    
                        for field in ['age', 'status', 'job', 'company', 'summary', 'project', 'socials']:
                            if request.POST.get(field):
                                setattr(member, field, request.POST.get(field))
                    
                        member.save()
                        response = {'status': 1}
    
    response = json.dumps(response, ensure_ascii=False)
    return HttpResponse(response, content_type="application/json; encoding=utf-8")


# Вывод рейтинга пользователей
def rating(request):
    response = []
    result = Member.objects.filter( ~Q(juror__id__gt = 0) & ~Q(city__exact = '') ).values('id', 'user__first_name', 'user__last_name', 'amount', 'likes', 'challenge_completed', 'city', 'photo').order_by('-amount')
    
    position = 0;
    for r in result:
        response.append({
            "position"              : position,
            "id"                    : r['id'],
            "first_name"            : r['user__first_name'],
            "last_name"             : r['user__last_name'],
            "materials"             : r['challenge_completed'],
            "amount"                : r['amount'],
            "likes"                 : r['likes'],
            "materials_declension"  : declension(r['challenge_completed'], "материал", "", "а", "ов"),
            "amount_declension"     : declension(r['amount'], "балл", "", "а", "ов"),
            "likes_declension"      : declension(r['likes'], "лайк", "", "а", "ов"),
            "image"                 : r['photo'],
            "city"                  : r['city'],            
        });
        position += 1
    response = json.dumps(response, ensure_ascii=False)
    return HttpResponse(response, content_type="application/json; encoding=utf-8")
    
# Вывод рейтинга geo
def geo_rating(request, field):
    
    values = [field]
    if field == 'city':
        values.append('position')        
        
    result = Member.objects.filter(~Q(juror__id__gt = 0)).exclude(**{field: ''}).values(*values).annotate(members=Count('id'), amount=Sum('amount'), tasks=Sum('challenge_completed') ).order_by('-amount')
    result_list = []
    n = 1
    
    for r in result:
        properties = {                
            "iconCaption"   : r[field] + " #" + str(n),                
            "members"       : str(r['members']) + " " + declension(r['members'], "участник", "", "а", "ов"),
            "amount"        : str(r['amount']) + " " + declension(r['amount'], "балл", "", "а", "ов"),
            "tasks"         : str(r['tasks']) + " " + declension(r['tasks'], "материал", "", "а", "ов")
        }
        
        if field == 'city':
            d = {
                "type": "Feature",
                "id": n,
                "geometry": {
                    "type" : "Point",                    
                },
                "properties": properties
            }
            if len(r['position']) > 0:
                d["geometry"]["coordinates"] = [float(i) for i in reversed(r['position'].split(" "))]                
            result_list.append(d)
        else:
            properties['name'] = r[field]
            result_list.append(properties)
            
        n += 1
    
    if field == 'city':    
        response = {
            "type": "FeatureCollection",
            "features": result_list
        }
    else:
        response = result_list
    
    response = json.dumps(response, ensure_ascii=False)
    return HttpResponse(response, content_type="application/json; encoding=utf-8")

# Удаление текущего сообщения пользователя
def clear_message(request):
    response = {'status': 0}
    if request.user.is_authenticated:        
        Member.objects.filter(user = request.user).update(message = '')    
    response['status'] = 1
    
    response = json.dumps(response, ensure_ascii=False)
    return HttpResponse(response, content_type="application/json; encoding=utf-8")

# Проверка рейтинга участника
def check_member_rating(current_member):
    members = Member.objects.all().order_by('-amount').only('amount', 'rating')
    ans = 0
    for index, member in enumerate(members):
        if member == current_member:
            new_index = index + 1
            if current_member.rating < new_index:
                ans = 1                
            elif current_member.rating > new_index:
                ans = -1            
            member.rating = new_index
            member.save()
            
    return ans