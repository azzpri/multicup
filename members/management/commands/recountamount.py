from challenges.models import Challenge, ChallengeHistory
from datetime import datetime, timezone
from django.core.management.base import BaseCommand, CommandError
from members.models import Member, Juror

class Command(BaseCommand):
    help = 'Пересчитать баллы участников на основе текущих показателей'

    def handle(self, *args, **options):
                
        members = Member.objects.exclude(id__in = [x.member.id for x in Juror.objects.only('member__id')])
        
        self.stdout.write(">>> Ставим 300 баллов всем:....\n")
        members.update(amount = 300)
        self.stdout.write("done!\n\n")
        
        self.stdout.write(">>> Добавляем баллы за задания challenge.total_amount to member.amount:....\n")
        for challenge in Challenge.objects.filter(total_amount__gt = 0):            
            challenge.member.amount = challenge.member.amount + challenge.total_amount
            self.stdout.write("add {0} for challenge {1} to {2} {3}\n".format(
                challenge.total_amount, challenge.id, challenge.member.user.first_name, challenge.member.user.last_name))
            challenge.member.save()
        self.stdout.write("done!\n\n")
        
        self.stdout.write(">>> Убираем/добавляем баллы за статусы Отклонено, Добавлено и Без материалов:....\n")
        members_wom = {}
        current_challenge_id = 0
        decline = False
        for history in ChallengeHistory.objects.order_by('challenge__id', 'changed_at'):
            log = ''
            action = 0
            if not history.challenge.member.id in members_wom:
                members_wom[history.challenge.member.id] = 0
            if history.challenge.id != current_challenge_id:
                current_challenge_id = history.challenge.id
                decline = False
            if history.status == 'wom':
                members_wom[history.challenge.member.id] += 1
                if members_wom[history.challenge.member.id] > 1:
                    action = -1                                
            elif history.status == 'decline':
                decline = True
                action = -1
            elif history.status == 'extra':
                if decline or members_wom[history.challenge.member.id] > 2:
                    action = 1
                    if decline:
                        decline = False
            if action == 1:
                history.challenge.member.amount = history.challenge.member.amount + history.challenge.task.cost
                self.stdout.write("add {0} for challenge {1} from {2} {3}\n".format(
                    history.challenge.task.cost, history.challenge.id, history.challenge.member.user.first_name, history.challenge.member.user.last_name))
                history.challenge.member.save()
            elif action == -1:
                history.challenge.member.amount = history.challenge.member.amount - history.challenge.task.cost
                history.challenge.member.save()
                self.stdout.write("remove {0} for challenge {1} from {2} {3}\n".format(
                    history.challenge.task.cost, history.challenge.id, history.challenge.member.user.first_name, history.challenge.member.user.last_name))                                    
                    
        self.stdout.write("done!\n\n")
        
        self.stdout.write(">>> Для контроля история статусов Пользователей:....\n")
        for member in members:
            self.stdout.write("> {0} {1}\n".format(member.user.first_name, member.user.last_name))
            for h in ChallengeHistory.objects.filter(challenge__member = member).order_by('challenge__id', 'changed_at'):                
                self.stdout.write("{0} {1}\n".format(h.challenge.id, h.status))
        self.stdout.write("done!\n\n")
                        
        success = "[{0}] Пересчет баллов успешно завершен!".format(str(datetime.now()))
        self.stdout.write(self.style.SUCCESS(success))