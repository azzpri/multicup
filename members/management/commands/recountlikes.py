import urllib, json
from challenges.views import check_active_challenge
from challenges.models import Challenge
from datetime import datetime, timezone
from django.core.mail import send_mail
from django.core.management.base import BaseCommand, CommandError
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import render
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils import timezone
from django_cron import CronJobBase, Schedule
from front.models import Setting
from members.models import Member, Juror, MemberAmountHistory
from math import log
from multicup import settings
from results.models import Result, ResultLikeHistory
from tasks.models import Task

#   Получить колличество лайков VK для Соревнования
#       @param challenge_id - ID Соревнования
#       @return int - количество лайков       
def getChallengeVKLikes(challenge_id):
    challenge_vk_likes = 0        
    material_url = settings.SERVER_URL + 'materials/' + str(challenge_id)
    params = urllib.parse.urlencode({'type': 'sitepage', 'owner_id': settings.SOCIAL_AUTH_VK_OAUTH2_KEY, 'page_url': material_url})
    try:
        f = urllib.request.urlopen(settings.VK_LIKE_API_URL + "?%s" % params)
        response = json.loads(f.read().decode('utf-8'))
        challenge_vk_likes = response['response']['count']
    except:
        pass        
    return challenge_vk_likes

#   Получить колличество лайков FB для Соревнования
#       @param challenge_id - ID Соревнования
#       @return int - количество лайков       
def getChallengeFBLikes(challenge_id):
    challenge_fb_likes = 0
    material_url = settings.SERVER_URL + 'materials/' + str(challenge_id)
    params = urllib.parse.urlencode({'ids': material_url})
    try:
        f = urllib.request.urlopen(settings.FB_GRAPH_API_URL + "?%s" % params)
        response = json.loads(f.read().decode('utf-8'))
        challenge_fb_likes = response[material_url]['share']['share_count']
    except:
        pass        
    return challenge_fb_likes

#   Пересчет баллов за лайки
class Command(BaseCommand):
    help = 'Пересчитать баллы за лайки'

    def handle(self, *args, **options):
        # получить все задачи с актуальными результатами, входящими в них, для соревнований в статусе принято
        results = Result.objects.filter(challenge__status = 'accepted').order_by('task__id', 'challenge__id', '-created_at')
        tasks = {}
        current_task_id = 0
        for result in results:
            if current_task_id != result.challenge.task.id:
                current_task_id = result.challenge.task.id
            if not current_task_id in tasks:
                tasks[current_task_id] = []
            
            if len(tasks[current_task_id]) > 0:
                if tasks[current_task_id][-1].challenge.id == result.challenge.id:
                    continue
            tasks[current_task_id].append(result)
                            
        # для каждой задачи
        for task_id, task_results in tasks.items():
            
            # найти наибольшее количество лайков для данной задачи
            task_max_likes = 0
            likes_changed = False
                        
            for index, result in enumerate(task_results):
                # получить количество лайков vk
                vk_likes = getChallengeVKLikes(result.challenge.id)
                
                # получить количество лайков fb
                fb_likes = getChallengeFBLikes(result.challenge.id)
                
                result_likes = vk_likes + fb_likes
                
                if result_likes > task_max_likes:
                    task_max_likes = result_likes
                
                if result_likes != result.likes:
                    task_results[index] = {
                        'challenge'     : result.challenge,
                        'member'        : result.member,
                        'result'        : result,
                        'likes'         : result_likes,
                        'cost'          : result.task.cost,                        
                    }    
                    likes_changed = True
            
            # если оно поменялось
            if likes_changed:
                for result in task_results:
                    if type(result).__name__ == 'dict':
                        if result['likes'] > 0:
                            like_amount = result['cost'] * (log(result['likes'], 1.5) + 1) / (log(task_max_likes, 1.5) + 1)
                        
                            if result['challenge'].likes_amount > like_amount:
                                diff = int(result['challenge'].likes_amount - like_amount)
                                result['member'].message = "Другие пользователи получили новые лайки за материалы. Вы потеряли {0} баллов".format(diff)
                                if diff != 0:
                                    amounthistory_action = 'remove'
                                    amounthistory_description = 'Пользователь потерял {0} баллов во время пересчета лайков'.format(diff)
                            else:
                                diff = int(like_amount - result['challenge'].likes_amount)
                                result['member'].message = "Вы получили новые лайки за материалы и {0} баллов".format(diff)
                                if diff != 0:
                                    amounthistory_action = 'add'
                                    amounthistory_description = 'Пользователь получил {0} баллов во время пересчета лайков'.format(diff)
                        
                            result['member'].amount = result['member'].amount - result['challenge'].likes_amount + like_amount
                            result['member'].save()
                            
                            result['result'].likes = result['likes']
                            result['result'].save()
                            result['challenge'].total_amount = result['challenge'].total_amount - result['challenge'].likes_amount + like_amount
                            result['challenge'].likes_amount = like_amount
                            result['challenge'].save()
    
                            if 'amounthistory_action' in locals():
                                amounthistory = MemberAmountHistory.objects.create(
                                    member = result['member'],
                                    action = amounthistory_action,
                                    description = amounthistory_description
                                ).save()
        
        success = "[{0}] Пересчет лайков успешно завершен!".format(str(datetime.now()))
        self.stdout.write(self.style.SUCCESS(success))