import urllib, json
from challenges.views import check_active_challenge
from challenges.models import Challenge
from datetime import datetime, timezone
from django.core.mail import send_mail
from django.core.management.base import BaseCommand, CommandError
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import render
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils import timezone
from django_cron import CronJobBase, Schedule
from front.models import Setting
from members.models import Member, Juror, MemberAmountHistory
from math import log
from multicup import settings
from results.models import Result, ResultLikeHistory
from tasks.models import Task


#   Пересчет рейтинга пользователей
#      @test members.test.test_managment_recounrating
class Command(BaseCommand):
    help = 'Пересчитать рейтинг для всех пользователей'

    def handle(self, *args, **options):        
        # Проход по Участникам для проверки задач с истекшим сроком и изменения рейтинга после пересчета лайков
        members = Member.objects.filter(~Q(juror__id__gt = 0) & ~Q(city__exact = '')).order_by('-amount').only('id', 'amount', 'message', 'rating', 'challenge_completed')
        challenges = Challenge.objects.filter(status__in = ['initial', 'extra']).order_by('member__amount').only('member')
        
        for index, member in enumerate(members):
            for challenge in challenges:
                if member == challenge.member:
                    check_result = check_active_challenge(False, challenge, notify_on_deadline = True)
                    
                    if not check_result and settings.MULTICUP['NOTIFICATIONS']['CHALLENGE_EXPIRE']:                
                        email_context = {
                            'server_url' : settings.SERVER_URL            
                        }
                        message = render_to_string('email/challenge-expire.html', email_context)
                        send_mail(
                            'Вы не успели опубликовать материалы к заданию',
                            message,
                            settings.EMAIL_HOST_USER,
                            [member.user.email, settings.EMAIL_HOST_USER],
                            html_message = message,
                        )
                        
            if member.rating != index:
                member.rating = index
                member.save()
        success = "[{0}] Пересчет рейтинга успешно завершен!".format(str(datetime.now()))
        self.stdout.write(self.style.SUCCESS(success))