# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-12-18 14:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0017_auto_20161215_2041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='subscription',
            field=models.BooleanField(default=True, verbose_name='Подписка'),
        ),
    ]
