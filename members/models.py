from django.contrib import auth
from django.contrib.auth.models import User
from django.db import models
from front.templatetags.multicup import declension
from multiselectfield import MultiSelectField
from tasks.models import FORMAT_CHOICES

def check_user_is_member(self):
    return Member.objects.filter(user = self).first()

def check_user_is_juror(self):
    return Juror.objects.filter(member__user = self).first()

auth.models.User.add_to_class('is_member', check_user_is_member)
auth.models.User.add_to_class('is_juror', check_user_is_juror)

class Member(models.Model):
    class Meta:
        verbose_name = 'Участник'
        verbose_name_plural = 'Участники'

    user = models.OneToOneField(User, on_delete = models.CASCADE, verbose_name = "Профиль пользователя")
    photo = models.FileField(blank = True, upload_to = 'uploads/avatars/', verbose_name = "Фото",
        help_text = "Фото должно быть квадратным ( не меньше 128х128px), в формате .jpeg или .png и размером до 2Мб")
    age = models.IntegerField(default = 0, verbose_name = "Возраст")
    city = models.CharField(blank = True, max_length = 30, verbose_name = "Город")
    region = models.CharField(blank = True, max_length = 50, verbose_name = "Регион")
    district = models.CharField(blank = True, max_length = 50, verbose_name = "Округ")
    district_osmid = models.IntegerField(default = 0, verbose_name = "osmId региона")
    geoobject = models.TextField(blank = True, verbose_name = "Yandex GeoObject")
    position = models.CharField(blank = True, max_length = 30, verbose_name = "Yandex GeoObject pos")
    status = models.CharField(blank = True, max_length = 100, verbose_name = "Професcия")
    job = models.CharField(blank = True, max_length = 100, verbose_name = "Должность")
    company = models.CharField(blank = True, max_length = 100, verbose_name = "Место работы")
    summary = models.CharField(blank = True, max_length = 500, verbose_name = "Краткий рассказ о себе")
    project = models.CharField(blank = True, max_length = 100, verbose_name = "Ссылка на сайт основного проекта")
    socials = models.TextField(blank = True, verbose_name = "Ссылки на социальные сети")
    amount = models.IntegerField(default = 0, verbose_name = "Количество баллов")
    rating = models.IntegerField(default = 0, verbose_name = "Рейтинг пользователя")
    likes = models.IntegerField(default = 0, verbose_name = "Количество лайков")
    challenge_completed = models.IntegerField(default = 0, verbose_name = "Количество выполненых заданий")
    subscription = models.BooleanField(default = True, verbose_name = "Подписка")
    message = models.CharField(blank = True, max_length = 100, verbose_name = "Сообщение для пользователя")
    
    def __str__(self):
        return self.user.first_name + " " + self.user.last_name
    
    def amount_declension(self):
        return str(self.amount) + " " + declension(self.amount, "балл", "", "а", "ов")
    
    def tasks(self):
        return str(self.challenge_completed) + " " + declension(self.challenge_completed, "задани", "е", "я", "й")
    
    def is_juror(self):
        return hasattr(self, 'juror')
    
    def photoname(self):
        if self.photo:
            return self.photo.name.split('/')[-1]
        else:
            return ''
            
class MemberAmountHistory(models.Model):
    class Meta:
        verbose_name = 'История баллов участника'
        verbose_name_plural = 'История баллов участников'
    
    ACTION_CHOICES = (
        ('add', 'Добавлено'),
        ('remove', 'Списано'),
    )
        
    member = models.ForeignKey(Member, on_delete = models.CASCADE)
    action = models.CharField(max_length = 30, choices = ACTION_CHOICES, verbose_name = "Операция")
    description = models.CharField(max_length = 512, verbose_name = "Описание") 
    changed_at = models.DateTimeField(auto_now_add = True, blank = True, verbose_name = "Дата изменения")


class Juror(models.Model):
    class Meta:
        verbose_name = 'Жюри'
        verbose_name_plural = 'Жюри'

    member = models.OneToOneField(Member, on_delete = models.CASCADE, verbose_name = "Участник")
    average_vote = models.FloatField(default = 0, verbose_name='Средний балл')
    experience = models.IntegerField(default = 0, verbose_name = "Стаж работы")
    amount = models.IntegerField(default = 0, verbose_name = 'Количество оцененных задач')
    formats = MultiSelectField(choices = FORMAT_CHOICES, verbose_name = 'Форматы задач')
    
    def __str__(self):
        return self.member.user.first_name + " " + self.member.user.last_name
    

class Region(models.Model):
    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'
        
    region_title = models.CharField(max_length=50, verbose_name="Название региона")
    district_title = models.CharField(max_length=50, verbose_name="Название округа")    